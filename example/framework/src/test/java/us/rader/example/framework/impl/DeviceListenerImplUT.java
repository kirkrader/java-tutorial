/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;

@Tag("unit-test")
@SuppressWarnings("javadoc")
public class DeviceListenerImplUT {

    @Test
    public void deviceClosing() throws Exception {

        final boolean[] notified = { false };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    device.addClosingListener(
                            sender -> notified[0] = true);

                }
            }
        }

        assertTrue(notified[0]);

    }

    @Test
    public void deviceNameChange() throws Exception {

        final String[] notified = { null };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    final BiConsumer<Device, String> callback = (
                            sender, newName) -> notified[0] = newName;
                    device.addNameChangeListener(callback);
                    device.setName("New Name 1");
                    assertEquals("New Name 1", device.getName());
                    device.removeNameChangeListener(callback);
                    device.setName("New Name 2");

                }
            }
        }

        assertEquals("New Name 1", notified[0]);

    }

    @Test
    public void deviceStateChange() throws Exception {

        final boolean[] notified = { false };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    final Consumer<Device> callback = sender -> notified[0] = true;
                    device.addStateChangeListener(callback);
                    final DeviceState state = new DeviceState();
                    state.put("on", true);
                    device.setState(state);
                    device.removeStateChangeListener(callback);
                    state.put("on", false);
                    device.setState(state);

                }
            }
        }

        assertTrue(notified[0]);

    }

    @Test
    public void getDevice() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    assertSame(device,
                            framework.getDevice("device1"));

                }

                assertNull(framework.getDevice("device1"));

            }
        }
    }

    @Test
    public void getDevices() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device1 = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockSwitchable device2 = new MockSwitchable(
                            plugin, "device2", "Device 2")) {

                        assertEquals(2,
                                framework.getDevices().length);

                    }

                    assertEquals(1, framework.getDevices().length);

                }

                assertEquals(0, framework.getDevices().length);

            }
        }
    }

    @Test
    public void getGroup() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockGroup group = new MockGroup(plugin,
                            "group1", "Group 1", device)) {

                        assertSame(group,
                                framework.getGroup("group1"));

                    }

                    assertNull(framework.getGroup("group1"));

                }
            }
        }
    }

    @Test
    public void getGroups() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockGroup group1 = new MockGroup(plugin,
                        "group1", "Group 1")) {

                    try (final MockGroup group2 = new MockGroup(
                            plugin, "group2", "Group 2")) {

                        assertEquals(2, framework.getGroups().length);

                    }

                    assertEquals(1, framework.getGroups().length);

                }

                assertEquals(0, framework.getGroups().length);

            }
        }
    }

    @Test
    public void getScene() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockGroup group = new MockGroup(plugin,
                            "group1", "Group 1", device)) {

                        try (final MockScene scene = new MockScene(
                                plugin, "scene1", "Scene 1",
                                "group1")) {

                            assertSame(scene, framework
                                    .getScene("scene1", "group1"));

                        }

                        assertNull(framework.getScene("scene1",
                                "group1"));

                    }
                }
            }
        }
    }

    @Test
    public void getScenes() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockGroup group = new MockGroup(plugin,
                        "group1", "Group 1")) {

                    try (final MockScene scene1 = new MockScene(
                            plugin, "scene1", "Scene 1", "group1")) {

                        try (final MockScene scene2 = new MockScene(
                                plugin, "scene2", "Scene 2",
                                "group1")) {

                            assertEquals(2,
                                    framework.getScenes().length);

                        }

                        assertEquals(1, framework.getScenes().length);

                    }

                    assertEquals(0, framework.getScenes().length);

                }
            }
        }
    }

    @Test
    public void groupClosing() throws Exception {

        final boolean[] notified = { false };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockGroup group = new MockGroup(plugin,
                            "group1", "Group 1")) {

                        group.addClosingListener(
                                sender -> notified[0] = true);

                    }
                }
            }
        }

        assertTrue(notified[0]);

    }

    @Test
    public void groupNameChange() throws Exception {

        final String[] notified = { null };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockGroup group = new MockGroup(plugin,
                            "group1", "Group 1")) {

                        final BiConsumer<Device, String> callback = (
                                sender,
                                newName) -> notified[0] = newName;
                        group.addNameChangeListener(callback);
                        group.setName("New Name 1");
                        assertEquals("New Name 1", group.getName());
                        group.removeNameChangeListener(callback);
                        group.setName("New Name 2");

                    }
                }
            }
        }

        assertEquals("New Name 1", notified[0]);

    }

    @Test
    public void groupStateChange() throws Exception {

        final boolean[] notified = { false };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockGroup group = new MockGroup(plugin,
                            "group1", "Group 1", device)) {

                        final Consumer<Device> callback = sender -> notified[0] = (boolean) sender
                                .getState().get("on");
                        group.addStateChangeListener(callback);
                        final DeviceState state = new DeviceState();
                        state.put("on", true);
                        device.setState(state);
                        assertTrue(notified[0]);
                        group.removeStateChangeListener(callback);
                        state.put("on", false);
                        device.setState(state);

                    }
                }
            }
        }

        assertTrue(notified[0]);

    }

    @Test
    public void registerDuplicateDevice() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try {

                    framework.registerDevice(new MockSwitchable(
                            plugin, "device1", "Device 1"));
                    fail("IllegalArgumentException expected");

                } catch (final IllegalArgumentException e) {

                    // expected exception

                }

                assertEquals(1, framework.getDevices().length);

            }

            assertEquals(0, framework.getDevices().length);

        }
    }

    @Test
    public void registerDuplicateGroup() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try {

                    framework.registerGroup(new MockGroup(plugin,
                            "group1", "Group 1"));
                    fail("IllegalArgumentException expected");

                } catch (final IllegalArgumentException e) {

                    // expected exception

                }

                assertEquals(1, framework.getGroups().length);

            }

            assertEquals(0, framework.getGroups().length);

        }
    }

    @Test
    public void registerDuplicateScene() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockGroup group = new MockGroup(plugin,
                        "group1", "Group 1")) {

                    try {

                        framework.registerScene(new MockScene(plugin,
                                "scene1", "Scene 1", "group1"));
                        fail("IllegalArgumentException expected");

                    } catch (final IllegalArgumentException e) {

                        // expected exception

                    }

                    assertEquals(1, framework.getScenes().length);

                }
            }

            assertEquals(0, framework.getScenes().length);

        }
    }

    @Test
    public void registerNullDevice() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try {

                    framework.registerDevice(null);
                    fail("NullPointerException expected");

                } catch (final NullPointerException e) {

                    // expected exception

                }

                assertEquals(0, framework.getDevices().length);

            }
        }
    }

    @Test
    public void registerNullGroup() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try {

                    framework.registerGroup(null);
                    fail("NullPointerException expected");

                } catch (final NullPointerException e) {

                    // expected exception

                }

                assertEquals(0, framework.getGroups().length);

            }
        }
    }

    @Test
    public void registerNullScene() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockGroup group = new MockGroup(plugin,
                        "group1", "Group 1")) {

                    try {

                        framework.registerScene(null);
                        fail("NullPointerException expected");

                    } catch (final NullPointerException e) {

                        // expected exception

                    }

                    assertEquals(0, framework.getScenes().length);

                }
            }
        }
    }

    @Test
    public void sceneClosing() throws Exception {

        final boolean[] notified = { false };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockGroup group = new MockGroup(plugin,
                            "group1", "Group 1")) {

                        try (final MockScene scene = new MockScene(
                                plugin, "scene1", "Scene 1",
                                "group1")) {

                            scene.addClosingListener(
                                    sender -> notified[0] = true);

                        }
                    }
                }
            }
        }

        assertTrue(notified[0]);

    }

    @Test
    public void sceneNameChange() throws Exception {

        final String[] notified = { null };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockGroup group = new MockGroup(plugin,
                            "group1", "Group 1")) {

                        try (final MockScene scene = new MockScene(
                                plugin, "scene1", "Scene 1",
                                "group1")) {

                            final BiConsumer<Device, String> callback = (
                                    sender,
                                    newName) -> notified[0] = newName;
                            scene.addNameChangeListener(callback);
                            scene.setName("New Name 1");
                            assertEquals("New Name 1",
                                    scene.getName());
                            scene.removeNameChangeListener(callback);
                            scene.setName("New Name 2");

                        }
                    }
                }
            }
        }

        assertEquals("New Name 1", notified[0]);

    }

    @Test
    public void sceneStateChange() throws Exception {

        final boolean[] notified = { false };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockGroup group = new MockGroup(plugin,
                            "group1", "Group 1", device)) {

                        try (final MockScene scene = new MockScene(
                                plugin, "scene1", "Scene 1",
                                "group1")) {

                            final Consumer<Device> callback = sender -> notified[0] = true;
                            scene.addStateChangeListener(callback);
                            scene.activate();

                        }
                    }
                }
            }
        }

        assertTrue(notified[0]);

    }
}
