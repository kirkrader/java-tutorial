/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("unit-test")
@SuppressWarnings("javadoc")
public class DeviceImplUT {

    @Test
    public void registerDevice() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    assertTrue(Arrays.stream(plugin.getDevices())
                            .anyMatch(element -> device == element));

                }

                assertEquals(0, plugin.getDevices().length);

            }
        }
    }
}
