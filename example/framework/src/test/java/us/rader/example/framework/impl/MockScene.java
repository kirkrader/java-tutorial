/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import java.io.Serializable;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;
import us.rader.example.framework.Group;
import us.rader.example.framework.Plugin;
import us.rader.example.framework.Scene;

/**
 * Mock {@link Scene}.
 */
public class MockScene extends SceneImpl {

    /**
     * Version constant required by {@link Serializable}.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The device settings recalled when this scene is activated.
     */
    private final DeviceState state;

    /**
     * Initialize super class.
     *
     * @param plugin  The {@link Plugin} which owns this {@link Scene}.
     *
     * @param id      Value for {@link Device#getId()}.
     *
     * @param name    Value for {@link Device#getName()}.
     *
     * @param groupId Value for {@link Scene#getGroupId()}.
     */
    public MockScene(final Plugin plugin, final String id,
            final String name, final String groupId) {

        super(plugin, id, name, groupId);
        state = new DeviceState();
        state.put("groupId", getGroupId());
        state.put("on", true);
        state.put("level", 0.5f);
        state.put("colorTemperature", 500);
        state.put("hue", 0.3333f);
        state.put("saturation", 0.6666f);

    }

    @Override
    public final synchronized void activate() throws Exception {

        final Group group = getPlugin().getGroup(getGroupId());
        final DeviceState state = getState();
        state.remove("groupId");

        for (final Device member : group.getMembers()) {

            state.put("id", member.getId());
            state.put("name", member.getName());
            member.setState(state);

        }

        super.activate();

    }

    @Override
    public synchronized DeviceState getState() {

        return state;

    }

    @Override
    public boolean update(final DeviceState state,
            final boolean changed) {

        boolean stateChanged = changed;

        final boolean oldOn = (Boolean) state.get("on");
        final float oldLevel = (Float) state.get("level");
        final int oldColorTemperature = (Integer) state
                .get("colorTemperature");
        final float oldHue = (Float) state.get("hue");
        final float oldSaturation = (Float) state.get("saturation");

        final boolean newOn = (Boolean) state.getOrDefault("on",
                oldOn);
        final float newLevel = (Float) state.getOrDefault("level",
                oldLevel);
        final int newColorTemperature = (Integer) state.getOrDefault(
                "colorTemperature", oldColorTemperature);
        final float newHue = (Float) state.getOrDefault("saturation",
                oldHue);
        final float newSaturation = (Float) state
                .getOrDefault("saturation", oldSaturation);

        if (!(oldOn == newOn)
                && Utilities.sameFloat(oldLevel, newLevel)
                && (oldColorTemperature == newColorTemperature)
                && Utilities.sameFloat(oldHue, newHue) && Utilities
                        .sameFloat(oldSaturation, newSaturation)) {

            this.state.put("on", newOn);
            this.state.put("level", newLevel);
            this.state.put("colorTemperature", newColorTemperature);
            this.state.put("hue", newHue);
            this.state.put("saturation", newSaturation);
            stateChanged = true;
        }

        return stateChanged;

    }
}