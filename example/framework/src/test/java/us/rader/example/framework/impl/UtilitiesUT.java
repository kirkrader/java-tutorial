/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@SuppressWarnings("javadoc")
@Tag("unit-test")
public class UtilitiesUT {

    @Test
    public void kelvinsToMireds() {
        assertEquals(40, Utilities.kelvinsToMireds(25000));
        assertEquals(200, Utilities.kelvinsToMireds(5000));
    }

    @Test
    public void miredsToKelvins() {
        assertEquals(25000, Utilities.miredsToKelvins(40));
        assertEquals(5000, Utilities.miredsToKelvins(200));
    }

    @Test
    public void sameFloat() {

        assertTrue(Utilities.sameFloat(0.0f, 0.0f));
        assertTrue(Utilities.sameFloat(0.0f, -0.0f));
        assertTrue(Utilities.sameFloat(1.0f, 1.0f));
        assertTrue(Utilities.sameFloat(1.01f, 1.01f));
        assertFalse(Utilities.sameFloat(1.0f, -1.0f));
        final float f2 = 1.0f + Math.ulp(1.0f);
        assertFalse(Utilities.sameFloat(1.0f, f2));

    }
}
