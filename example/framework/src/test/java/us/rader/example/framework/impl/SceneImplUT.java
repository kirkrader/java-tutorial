/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("unit-test")
@SuppressWarnings("javadoc")
public class SceneImplUT {

    private static void activateDevices(final MockPlugin plugin)
            throws Exception {

        try (final MockSwitchable switchable = new MockSwitchable(
                plugin, "device1", "Switchable")) {

            try (final MockDimmable dimmable = new MockDimmable(
                    plugin, "device2", "Dimmable")) {

                try (final MockMiredTunable miredTunable = new MockMiredTunable(
                        plugin, "device3", "MireTunable")) {

                    try (final MockColorable colorable = new MockColorable(
                            plugin, "device4", "Colorable")) {

                        activateGroup(plugin, switchable, dimmable,
                                miredTunable, colorable);

                    }
                }
            }
        }
    }

    private static void activateGroup(final MockPlugin plugin,
            final MockSwitchable switchable,
            final MockDimmable dimmable,
            final MockMiredTunable miredTunable,
            final MockColorable colorable) throws Exception {
        try (final MockGroup group = new MockGroup(plugin, "group1",
                "Group 1", switchable, dimmable, miredTunable,
                colorable)) {

            activateScene(plugin, switchable, dimmable, miredTunable,
                    colorable);

        }
    }

    private static void activateScene(final MockPlugin plugin,
            final MockSwitchable switchable,
            final MockDimmable dimmable,
            final MockMiredTunable miredTunable,
            final MockColorable colorable) throws Exception {

        try (final MockScene scene = new MockScene(plugin, "scene1",
                "Scene 1", "group1")) {

            scene.activate();
            assertTrue((boolean) switchable.getState().get("on"));
            assertTrue((boolean) dimmable.getState().get("on"));
            assertTrue((boolean) miredTunable.getState().get("on"));
            assertTrue((boolean) colorable.getState().get("on"));
            assertTrue(Utilities.sameFloat(0.5f,
                    (float) dimmable.getState().get("level")));
            assertTrue(Utilities.sameFloat(0.5f,
                    (float) miredTunable.getState().get("level")));
            assertTrue(Utilities.sameFloat(0.5f,
                    (float) colorable.getState().get("level")));
            assertEquals(500, (int) miredTunable.getState()
                    .get("colorTemperature"));
            assertEquals(500, (int) colorable.getState()
                    .get("colorTemperature"));
            assertTrue(Utilities.sameFloat(0.3333f,
                    (float) colorable.getState().get("hue")));
            assertTrue(Utilities.sameFloat(0.6666f,
                    (float) colorable.getState().get("saturation")));

        }
    }

    @Test
    public void activate() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                activateDevices(plugin);

            }
        }
    }

    @Test
    public void getGroupId() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockGroup group = new MockGroup(plugin,
                        "group1", "Group 1")) {

                    try (final MockScene scene = new MockScene(plugin,
                            "scene1", "Scene 1", "group1")) {

                        assertEquals("group1", scene.getGroupId());

                    }
                }
            }
        }
    }

    @Test
    public void nullGroupId() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockScene scene = new MockScene(plugin,
                        "scene1", "Scene 1", null)) {

                    fail("NullPointerException expected");

                } catch (final NullPointerException e) {

                    // expected exception

                }
            }
        }
    }
}