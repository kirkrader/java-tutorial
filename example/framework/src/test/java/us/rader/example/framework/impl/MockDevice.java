/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import java.io.Serializable;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;
import us.rader.example.framework.Plugin;

/**
 * Mock {@link Device}.
 */
public class MockDevice extends DeviceCommon {

    /**
     * Version constant required by {@link Serializable}.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Mock device state.
     */
    private boolean value;

    /**
     * Initialize super class.
     *
     * @param plugin The {@link Plugin} which owns this device.
     *
     * @param id     Value for {@link Device#getId()}.
     *
     * @param name   Value for {@link Device#getName()}.
     */
    public MockDevice(final Plugin plugin, final String id,
            final String name) {

        super(plugin, id, name);

    }

    @Override
    public synchronized DeviceState getState() {

        final DeviceState state = super.getState();
        state.put("value", value);
        return state;

    }

    @Override
    public boolean update(final DeviceState state,
            final boolean changed) {

        boolean stateChanged = changed;
        final Boolean value = (Boolean) state.getOrDefault("value",
                this.value);

        if (value != this.value) {

            this.value = value;
            stateChanged = true;

        }

        return stateChanged;

    }
}