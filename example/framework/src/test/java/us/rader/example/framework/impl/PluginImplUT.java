/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("unit-test")
@SuppressWarnings("javadoc")
public class PluginImplUT {

    @Test
    @SuppressWarnings("resource")
    public void close() {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                final MockSwitchable device1 = new MockSwitchable(
                        plugin, "device1", "Device 1");
                new MockGroup(plugin, "group1", "Group 1", device1);
                new MockScene(plugin, "scene1", "Scene 1", "group1");

            }

        } catch (final Exception e) {

            fail("unexpected exception");

        }
    }

    @Test
    public void getFramework() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                assertSame(framework, plugin.getFramework());

            }
        }
    }
}