/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import us.rader.example.framework.Framework;
import us.rader.example.framework.Plugin;

/**
 * Mock {@link Plugin}.
 */
public class MockPlugin extends PluginImpl {

    /**
     * Initialize super class.
     *
     * @param framework The activate {@link Framework}.
     */
    public MockPlugin(final Framework framework) {

        super(framework);

    }
}