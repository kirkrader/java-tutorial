/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import us.rader.example.framework.impl.SceneKey;

@SuppressWarnings("javadoc")
@Tag("unit-test")
public class SceneKeyUT {

    @Test
    public void equalsTest() {

        assertEquals(new SceneKey("1", "one"),
                new SceneKey("1", "one"));

        assertEquals(new SceneKey("2", "two"),
                new SceneKey("2", "two"));

        assertNotEquals(new SceneKey("1", "one"),
                new SceneKey("2", "two"));

        assertNotEquals(new SceneKey("1", "one"), "1");

        assertNotEquals(new SceneKey("1", "one"), null);

    }

    @Test
    public void hashCodeTest() {

        assertEquals(new SceneKey("1", "one").hashCode(),
                new SceneKey("1", "one").hashCode());

        assertEquals(new SceneKey("2", "two").hashCode(),
                new SceneKey("2", "two").hashCode());

        assertNotEquals(new SceneKey("1", "one").hashCode(),
                new SceneKey("2", "two").hashCode());

    }

    @Test
    public void nullGroupId() {

        try {

            new SceneKey("scene", null);
            fail("NullPointerException expected");

        } catch (final NullPointerException e) {

            // expected exception

        }
    }

    @Test
    public void nullSceneId() {

        try {

            new SceneKey(null, "group");
            fail("NullPointerException expected");

        } catch (final NullPointerException e) {

            // expected exception

        }
    }

    @Test
    public void toStringTest() {

        assertNotNull(new SceneKey("1", "one").toString());

    }
}
