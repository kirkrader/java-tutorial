/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;

@Tag("unit-test")
@SuppressWarnings("javadoc")
public class GroupImplUT {

    @Test
    public void addDuplicateMember() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device1 = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockGroup group = new MockGroup(plugin,
                            "group1", "Group 1", device1)) {

                        try {

                            group.addMember(device1);
                            fail("IllegalArgumentException expected");

                        } catch (final IllegalArgumentException e) {

                            // expected exception

                        }
                    }
                }
            }
        }
    }

    @Test
    public void addNullMember() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockGroup group = new MockGroup(plugin,
                        "group1", "Group 1")) {

                    try {

                        group.addMember(null);
                        fail("NullPointerException expected");

                    } catch (final NullPointerException e) {

                        // expected exception

                    }
                }
            }
        }
    }

    @Test
    public void closeMember() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockGroup group = new MockGroup(plugin,
                        "group1", "Group 1")) {

                    try (final MockSwitchable device = new MockSwitchable(
                            plugin, "device1", "Device 1")) {

                        group.addMember(device);
                        final Device[] members = group.getMembers();
                        assertEquals(1, members.length);
                        assertSame(device, members[0]);

                    }

                    assertEquals(0, group.getMembers().length);

                }
            }
        }
    }

    @Test
    public void dimmableStateChange() throws Exception {

        final boolean[] notified = { false };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDimmable device1 = new MockDimmable(
                        plugin, "device1", "Device 1")) {

                    try (final MockDimmable device2 = new MockDimmable(
                            plugin, "device2", "Device 2")) {

                        try (final MockGroup group = new MockGroup(
                                plugin, "group1", "Group 1", device1,
                                device2)) {

                            group.addStateChangeListener(
                                    sender -> notified[0] = true);

                            assertTrue(Utilities.sameFloat(0.0f,
                                    (float) group.getState()
                                            .get("level")));
                            final DeviceState state = new DeviceState();
                            state.put("on", true);
                            state.put("level", 1.0f);
                            device1.setState(state);
                            assertTrue(Utilities.sameFloat(0.5f,
                                    (float) group.getState()
                                            .get("level")));

                        }
                    }
                }
            }
        }

        assertTrue(notified[0]);

    }

    @Test
    public void getMembers() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device1 = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockGroup group = new MockGroup(plugin,
                            "group1", "Group 1", device1)) {

                        final Device[] members = group.getMembers();
                        assertEquals(1, members.length);
                        assertSame(device1, members[0]);

                    }
                }
            }
        }
    }

    @Test
    public void hueChange() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockColorable device1 = new MockColorable(
                        plugin, "device1", "Device 1")) {

                    try (final MockColorable device2 = new MockColorable(
                            plugin, "device2", "Device 2")) {

                        try (final MockGroup group = new MockGroup(
                                plugin, "group1", "Group 1", device1,
                                device2)) {

                            assertTrue(Utilities.sameFloat(0.0f,
                                    (float) group.getState()
                                            .get("hue")));
                            final DeviceState state = new DeviceState();
                            state.put("on", true);
                            state.put("level", 1.0f);
                            state.put("hue", 1.0f);
                            state.put("saturation", 0.0f);
                            device2.setState(state);
                            assertTrue(Utilities.sameFloat(0.5f,
                                    (float) group.getState()
                                            .get("hue")));

                        }
                    }
                }
            }
        }
    }

    @Test
    public void miredTunableStateChange() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockMiredTunable device1 = new MockMiredTunable(
                        plugin, "device1", "Device 1")) {

                    try (final MockMiredTunable device2 = new MockMiredTunable(
                            plugin, "device2", "Device 2")) {

                        try (final MockGroup group = new MockGroup(
                                plugin, "group1", "Group 1", device1,
                                device2)) {

                            assertEquals(0, (int) group.getState()
                                    .get("colorTemperature"));
                            final DeviceState state = new DeviceState();
                            state.put("on", true);
                            state.put("level", 1.0f);
                            state.put("colorTemperature", 100);
                            device2.setState(state);
                            assertEquals(50, (int) group.getState()
                                    .get("colorTemperature"));

                        }
                    }
                }
            }
        }
    }

    @Test
    public void saturationChange() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockColorable device1 = new MockColorable(
                        plugin, "device1", "Device 1")) {

                    try (final MockColorable device2 = new MockColorable(
                            plugin, "device2", "Device 2")) {

                        try (final MockGroup group = new MockGroup(
                                plugin, "group1", "Group 1", device1,
                                device2)) {

                            assertTrue(Utilities.sameFloat(0.0f,
                                    (float) group.getState()
                                            .get("saturation")));
                            final DeviceState state = new DeviceState();
                            state.put("on", true);
                            state.put("level", 1.0f);
                            state.put("hue", 0.0f);
                            state.put("saturation", 1.0f);
                            device2.setState(state);
                            assertTrue(Utilities.sameFloat(0.5f,
                                    (float) group.getState()
                                            .get("saturation")));

                        }
                    }
                }
            }
        }
    }

    @Test
    public void setDimmableState() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDimmable device1 = new MockDimmable(
                        plugin, "device1", "Device 1")) {

                    try (final MockMiredTunable device2 = new MockMiredTunable(
                            plugin, "device2", "Device 2")) {

                        try (final MockGroup group = new MockGroup(
                                plugin, "group1", "Group 1", device1,
                                device2)) {

                            assertTrue(Utilities.sameFloat(0.0f,
                                    (float) device1.getState()
                                            .get("level")));
                            assertTrue(Utilities.sameFloat(0.0f,
                                    (float) device2.getState()
                                            .get("level")));
                            final DeviceState state = new DeviceState();
                            state.put("on", true);
                            state.put("level", 0.3333f);
                            group.setState(state);
                            assertTrue(Utilities.sameFloat(0.3333f,
                                    (float) device1.getState()
                                            .get("level")));
                            assertTrue(Utilities.sameFloat(0.3333f,
                                    (float) device2.getState()
                                            .get("level")));

                        }
                    }
                }
            }
        }
    }

    @Test
    public void setHue() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockColorable device1 = new MockColorable(
                        plugin, "device1", "Device 1")) {

                    try (final MockColorable device2 = new MockColorable(
                            plugin, "device2", "Device 2")) {

                        try (final MockGroup group = new MockGroup(
                                plugin, "group1", "Group 1", device1,
                                device2)) {

                            assertTrue(Utilities.sameFloat(0.0f,
                                    (float) device1.getState()
                                            .get("hue")));
                            assertTrue(Utilities.sameFloat(0.0f,
                                    (float) device2.getState()
                                            .get("hue")));
                            final DeviceState state = new DeviceState();
                            state.put("on", true);
                            state.put("level", 1.0f);
                            state.put("hue", 0.3333f);
                            state.put("saturation", 1.0f);
                            group.setState(state);
                            assertTrue(Utilities.sameFloat(0.3333f,
                                    (float) device1.getState()
                                            .get("hue")));
                            assertTrue(Utilities.sameFloat(0.3333f,
                                    (float) device2.getState()
                                            .get("hue")));

                        }
                    }
                }
            }
        }
    }

    @Test
    public void setMiredTunableState() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockMiredTunable device1 = new MockMiredTunable(
                        plugin, "device1", "Device 1")) {

                    try (final MockColorable device2 = new MockColorable(
                            plugin, "device2", "Device 2")) {

                        try (final MockGroup group = new MockGroup(
                                plugin, "group1", "Group 1", device1,
                                device2)) {

                            assertEquals(0, (int) device1.getState()
                                    .get("colorTemperature"));
                            assertEquals(0, (int) device2.getState()
                                    .get("colorTemperature"));
                            final DeviceState state = new DeviceState();
                            state.put("on", true);
                            state.put("level", 1.0f);
                            state.put("colorTemperature", 500);
                            group.setState(state);
                            assertEquals(500, (int) device1.getState()
                                    .get("colorTemperature"));
                            assertEquals(500, (int) device2.getState()
                                    .get("colorTemperature"));

                        }
                    }
                }
            }
        }
    }

    @Test
    public void setSaturation() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockColorable device1 = new MockColorable(
                        plugin, "device1", "Device 1")) {

                    try (final MockColorable device2 = new MockColorable(
                            plugin, "device2", "Device 2")) {

                        try (final MockGroup group = new MockGroup(
                                plugin, "group1", "Group 1", device1,
                                device2)) {

                            assertTrue(Utilities.sameFloat(0.0f,
                                    (float) device1.getState()
                                            .get("saturation")));
                            assertTrue(Utilities.sameFloat(0.0f,
                                    (float) device2.getState()
                                            .get("saturation")));
                            final DeviceState state = new DeviceState();
                            state.put("on", true);
                            state.put("level", 1.0f);
                            state.put("hue", 1.0f);
                            state.put("saturation", 0.3333f);
                            group.setState(state);
                            assertTrue(Utilities.sameFloat(0.3333f,
                                    (float) device1.getState()
                                            .get("saturation")));
                            assertTrue(Utilities.sameFloat(0.3333f,
                                    (float) device2.getState()
                                            .get("saturation")));

                        }
                    }
                }
            }
        }
    }

    @Test
    public void setSwitchableState() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device1 = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockDimmable device2 = new MockDimmable(
                            plugin, "device2", "Device 2")) {

                        try (final MockGroup group = new MockGroup(
                                plugin, "group1", "Group 1", device1,
                                device2)) {

                            assertFalse((boolean) device1.getState()
                                    .get("on"));
                            assertFalse((boolean) device2.getState()
                                    .get("on"));
                            final DeviceState state = new DeviceState();
                            state.put("on", true);
                            group.setState(state);
                            assertTrue((boolean) device1.getState()
                                    .get("on"));
                            assertTrue((boolean) device2.getState()
                                    .get("on"));

                        }
                    }
                }
            }
        }
    }

    @Test
    public void switchableStateChange() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockSwitchable device1 = new MockSwitchable(
                        plugin, "device1", "Device 1")) {

                    try (final MockDimmable device2 = new MockDimmable(
                            plugin, "device2", "Device 2")) {

                        try (final MockGroup group = new MockGroup(
                                plugin, "group1", "Group 1", device1,
                                device2)) {

                            assertFalse((boolean) group.getState()
                                    .get("on"));
                            final DeviceState state = new DeviceState();
                            state.put("on", true);
                            state.put("level", 0.5f);
                            device2.setState(state);
                            assertTrue((boolean) group.getState()
                                    .get("on"));

                        }
                    }
                }
            }
        }
    }
}