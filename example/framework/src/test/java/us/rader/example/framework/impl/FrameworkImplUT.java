/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("unit-test")
@SuppressWarnings("javadoc")
public class FrameworkImplUT {

    @Test
    public void addDuplicatePlugin() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try {

                    framework.addPlugin(plugin);
                    fail("IllegalArgumentException expected");

                } catch (final IllegalArgumentException e) {

                    // expected exception

                }
            }
        }
    }

    @Test
    public void addIfNonPlugin() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            assertNull(framework.addIfPlugin(Object.class));

        }
    }

    @Test
    public void addIfPlugin() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = (MockPlugin) framework
                    .addIfPlugin(MockPlugin.class)) {

                assertNotNull(plugin);
                assertTrue(Arrays.stream(framework.getPlugins())
                        .anyMatch(element -> element == plugin));

            }

            assertEquals(0, framework.getPlugins().length);

        }
    }

    @Test
    public void addNullPlugin() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try {

                framework.addPlugin(null);
                fail("NullPointerException expected");

            } catch (final NullPointerException e) {

                // expected exception

            }
        }
    }

    @Test
    @SuppressWarnings("resource")
    public void close() {

        try (final MockFramework framework = new MockFramework()) {

            new MockPlugin(framework);

        } catch (final Exception e) {

            fail("unexpected exception");

        }
    }

}