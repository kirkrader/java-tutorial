/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;

@Tag("unit-test")
@SuppressWarnings("javadoc")
public class DeviceCommonUT {

    @Test
    public void addClosingListener() throws Exception {

        final boolean notified[] = { false };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    device.addClosingListener(
                            sender -> notified[0] = true);

                }
            }
        }

        assertTrue(notified[0]);

    }

    @Test
    public void addDuplicateClosingListener() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    try {

                        final Consumer<Device> callback = (sender -> {
                            sender.toString();
                        });

                        try {

                            device.addClosingListener(callback);

                        } catch (final Exception e) {

                            fail("unexpected exception");

                        }

                        device.addClosingListener(callback);
                        fail("IllegalArgumentException expected");

                    } catch (final IllegalArgumentException e) {

                        // expected exception

                    }
                }
            }
        }
    }

    @Test
    public void addDuplicateNameChangeListener() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    try {

                        final BiConsumer<Device, String> callback = ((
                                sender, newName) -> {
                            sender.toString();
                        });

                        try {

                            device.addNameChangeListener(callback);

                        } catch (final Exception e) {

                            fail("unexpected exception");

                        }

                        device.addNameChangeListener(callback);
                        fail("IllegalArgumentException expected");

                    } catch (final IllegalArgumentException e) {

                        // expected exception

                    }
                }
            }
        }
    }

    @Test
    public void addDuplicateStateChangeListener() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    try {

                        final Consumer<Device> callback = (sender -> {
                            sender.toString();
                        });

                        try {

                            device.addStateChangeListener(callback);

                        } catch (final Exception e) {

                            fail("unexpected exception");

                        }

                        device.addStateChangeListener(callback);
                        fail("IllegalArgumentException expected");

                    } catch (final IllegalArgumentException e) {

                        // expected exception

                    }
                }
            }
        }
    }

    @Test
    public void addNameChangeListener() throws Exception {

        final String notified[] = { null };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    device.addNameChangeListener((sender,
                            newName) -> notified[0] = newName);
                    device.setName("New Name");

                }
            }
        }

        assertEquals("New Name", notified[0]);

    }

    @Test
    public void addNullClosingListener() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    try {

                        device.addClosingListener(null);
                        fail("NullPointerException expected");

                    } catch (final NullPointerException e) {

                        // expected exception

                    }
                }
            }
        }
    }

    @Test
    public void addNullNameChangeListener() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    try {

                        device.addNameChangeListener(null);
                        fail("NullPointerException expected");

                    } catch (final NullPointerException e) {

                        // expected exception

                    }
                }
            }
        }
    }

    @Test
    public void addNullStateChangeListener() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    try {

                        device.addStateChangeListener(null);
                        fail("NullPointerException expected");

                    } catch (final NullPointerException e) {

                        // expected exception

                    }
                }
            }
        }
    }

    @Test
    public void addStateChangeListener() throws Exception {

        final boolean notified[] = { false };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    device.addStateChangeListener(
                            sender -> notified[0] = (boolean) sender
                                    .getState().get("value"));

                    final DeviceState state = new DeviceState();
                    state.put("value", true);
                    device.setState(state);

                }
            }
        }

        assertTrue(notified[0]);

    }

    @Test
    public void getId() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    assertEquals("device1", device.getId());

                }
            }
        }
    }

    @Test
    public void getName() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    assertEquals("Device 1", device.getName());
                    device.setName("New Name");
                    assertEquals("New Name", device.getName());

                }
            }
        }
    }

    @Test
    public void getPlugin() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    assertSame(plugin, device.getPlugin());

                }
            }
        }
    }

    @Test
    public void getState() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    final DeviceState state = device.getState();
                    assertEquals("device1", state.get("id"));
                    assertEquals("Device 1", state.get("name"));
                    assertFalse((Boolean) state.get("value"));
                    state.put("value", true);
                    device.setState(state);
                    assertTrue(
                            (Boolean) device.getState().get("value"));

                }
            }
        }
    }

    @Test
    public void nullId() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        null, "name")) {

                    fail("NullPointerException expected");

                } catch (final NullPointerException e) {

                    // expected exception

                }
            }
        }
    }

    @Test
    public void nullName() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "id", null)) {

                    fail("NullPointerException expected");

                } catch (final NullPointerException e) {

                    // expected exception

                }
            }
        }
    }

    @Test
    public void nullPlugin() throws Exception {

        try (final MockDevice device = new MockDevice(null, "id",
                "name")) {

            fail("NullPointerException expected");

        } catch (final NullPointerException e) {

            // expected exception

        }
    }

    @Test
    public void removeClosingListener() throws Exception {

        final boolean notified[] = { false };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    final Consumer<Device> callback = sender -> notified[0] = true;
                    device.addClosingListener(callback);
                    device.removeClosingListener(callback);

                }
            }
        }

        assertFalse(notified[0]);

    }

    @Test
    public void removeNameChangeListener() throws Exception {

        final String notified[] = { null };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    final BiConsumer<Device, String> callback = (
                            sender, newName) -> notified[0] = newName;
                    device.addNameChangeListener(callback);
                    device.removeNameChangeListener(callback);
                    device.setName("New Name");

                }
            }
        }

        assertNull(notified[0]);

    }

    @Test
    public void removeStateChangeListener() throws Exception {

        final boolean notified[] = { false };

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    final Consumer<Device> callback = sender -> notified[0] = (Boolean) sender
                            .getState().get("value");
                    device.addStateChangeListener(callback);
                    device.removeStateChangeListener(callback);
                    final DeviceState state = new DeviceState();
                    state.put("value", true);
                    device.setState(state);

                }
            }
        }

        assertFalse(notified[0]);

    }

    @Test
    public void setNullName() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final MockPlugin plugin = new MockPlugin(
                    framework)) {

                try (final MockDevice device = new MockDevice(plugin,
                        "device1", "Device 1")) {

                    try {

                        device.setName(null);
                        fail("NullPointerException expected");

                    } catch (final NullPointerException e) {

                        // expected exception

                    }
                }
            }
        }
    }
}
