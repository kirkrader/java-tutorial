/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Abstract classes providing core functionality declared by inversion of
 * control framework interfaces.
 *
 * The abstract classes in the package are intended to be extended by plugin
 * modules. When doing so, methods that are not marked {@code final} should
 * generally invoke their super-class implementations in adition to performing
 * any plugin-specific logic.
 *
 * For example, when overriding
 * {@link us.rader.example.framework.impl.DeviceImpl#getState()}, you should add
 * plugin-specific state values to a
 * {@link us.rader.example.framework.DeviceState} instance obtained by calling
 * {@code super.getState()}. Similarly, when overriding
 * {@link us.rader.example.framework.impl.SceneImpl#activate()} you should call
 * {@code super.activate()} after performing any plugin-specific activities. The
 * same pattern applies throughout the methods in all the classes in this
 * package.
 */
package us.rader.example.framework.impl;
