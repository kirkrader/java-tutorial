package us.rader.example.framework.impl;

import static java.util.logging.Level.WARNING;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.logging.Logger;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;
import us.rader.example.framework.Plugin;

/**
 * Behavior common to all types that implement {@link Device}.
 */
public abstract class DeviceCommon implements Device {

    /**
     * {@link Logger} used for debugging.
     */
    private static final Logger logger;

    /**
     * Version constant required by {@link java.io.Serializable}.
     */
    private static final long serialVersionUID;

    static {

        serialVersionUID = 1L;
        logger = Logger.getLogger(DeviceCommon.class.getName());

    }

    /**
     * Add the given callback to the given list.
     *
     * @param callback  The callback.
     *
     * @param callbacks The list.
     */
    private static void addListener(
            final BiConsumer<Device, String> callback,
            final List<BiConsumer<Device, String>> callbacks) {

        if (callback == null) {

            throw new NullPointerException("null callback");

        }

        synchronized (callbacks) {

            if (callbacks.contains(callback)) {

                throw new IllegalArgumentException(
                        "duplicate callback");

            }

            callbacks.add(callback);

        }
    }

    /**
     * Add the given callback to the given list.
     *
     * @param callback  The callback.
     *
     * @param callbacks The list.
     */
    private static void addListener(final Consumer<Device> callback,
            final List<Consumer<Device>> callbacks) {

        if (callback == null) {

            throw new NullPointerException("null callback");

        }

        synchronized (callbacks) {

            if (callbacks.contains(callback)) {

                throw new IllegalArgumentException(
                        "duplicate callback");

            }

            callbacks.add(callback);

        }
    }

    /**
     * Remove the given callback from the given list.
     *
     * @param callback  The callback.
     *
     * @param callbacks The list.
     */
    private static void removeListener(
            final BiConsumer<Device, String> callback,
            final List<BiConsumer<Device, String>> callbacks) {

        synchronized (callbacks) {

            callbacks.remove(callback);

        }
    }

    /**
     * Remove the given callback from the given list.
     *
     * @param callback  The callback.
     *
     * @param callbacks The list.
     */
    private static void removeListener(
            final Consumer<Device> callback,
            final List<Consumer<Device>> callbacks) {

        synchronized (callbacks) {

            callbacks.remove(callback);

        }
    }

    /**
     * The callbacks to invoke when this device's close method is called.
     */
    private final List<Consumer<Device>> closingListeners;

    /**
     * The device's immuatable identifier string.
     *
     * @see #getId()
     */
    private final String id;

    /**
     * The device's mutable name string.
     *
     * @see #getName()
     * @see #setName(String)
     */
    private String name;

    /**
     * The callbacks to invoke when this device's name is changed.
     */
    private final List<BiConsumer<Device, String>> nameChangeListeners;

    /**
     * The {@link Plugin} responseible for this device.
     */
    private final Plugin plugin;

    /**
     * List of clients to notify when this device's state changes.
     */
    private final List<Consumer<Device>> stateChangeListeners;

    /**
     * Initialize {@link Device} interface behavior.
     *
     * @param plugin The {@link Plugin} responsible for this device.
     *
     * @param id     Value for {@link Device#getId()}.
     *
     * @param name   Value for {@link Device#getName()}.
     */
    public DeviceCommon(final Plugin plugin, final String id,
            final String name) {

        if (plugin == null) {

            throw new NullPointerException("null plugin");

        }

        if (id == null) {

            throw new NullPointerException("null id");

        }

        if (name == null) {

            throw new NullPointerException("null name");

        }

        this.plugin = plugin;
        this.id = id;
        this.name = name;
        stateChangeListeners = new LinkedList<>();
        nameChangeListeners = new LinkedList<>();
        closingListeners = new LinkedList<>();

    }

    @Override
    public final void addClosingListener(
            final Consumer<Device> callback) {

        addListener(callback, closingListeners);

    }

    @Override
    public final void addNameChangeListener(
            final BiConsumer<Device, String> callback) {

        addListener(callback, nameChangeListeners);

    }

    @Override
    public final void addStateChangeListener(
            final Consumer<Device> callback) {

        addListener(callback, stateChangeListeners);

    }

    @Override
    public synchronized void close() throws Exception {

        notifyListeners(closingListeners);
        closingListeners.clear();
        stateChangeListeners.clear();
        nameChangeListeners.clear();

    }

    @Override
    public synchronized final String getId() {

        return id;

    }

    @Override
    public synchronized final String getName() {

        return name;

    }

    @Override
    public final Plugin getPlugin() {

        return plugin;

    }

    @Override
    public synchronized DeviceState getState() {

        final DeviceState state = new DeviceState();
        state.put("id", id);
        state.put("name", name);
        return state;

    }

    /**
     * Invoke each of the callbacks in the given list.
     *
     * @param listeners The list of callbacks.
     *
     * @param newValue  The second argument for each callback.
     */
    private void notifyListeners(
            final List<BiConsumer<Device, String>> listeners,
            final String newValue) {

        synchronized (listeners) {

            listeners.forEach(callback -> {

                try {

                    callback.accept(this, newValue);

                } catch (final Exception e) {

                    if (logger.isLoggable(WARNING)) {

                        logger.log(WARNING, e.getMessage(), e);

                    }
                }
            });

        }
    }

    /**
     * Invoke each of the callbacks in the given list.
     *
     * @param listeners The list of callbacks.
     */
    private void notifyListeners(
            final List<Consumer<Device>> listeners) {

        synchronized (listeners) {

            listeners.forEach(callback -> {

                try {

                    callback.accept(this);

                } catch (final Exception e) {

                    if (logger.isLoggable(WARNING)) {

                        logger.log(WARNING, e.getMessage(), e);

                    }
                }
            });

        }
    }

    /**
     * Send event to all state-change listeners.
     *
     * @see #addStateChangeListener(Consumer)
     * @see #removeStateChangeListener(Consumer)
     */
    protected final void notifyStateChangeListeners() {

        notifyListeners(stateChangeListeners);

    }

    @Override
    public final void removeClosingListener(
            final Consumer<Device> callback) {

        removeListener(callback, closingListeners);

    }

    @Override
    public final void removeNameChangeListener(
            final BiConsumer<Device, String> callback) {

        removeListener(callback, nameChangeListeners);

    }

    @Override
    public final void removeStateChangeListener(
            final Consumer<Device> callback) {

        removeListener(callback, stateChangeListeners);

    }

    @Override
    public synchronized void setName(final String name) {

        if (name == null) {

            throw new NullPointerException("null name");
        }

        if (!name.equals(this.name)) {

            notifyListeners(nameChangeListeners, name);
            this.name = name;

        }
    }

    @Override
    public final void setState(final DeviceState state) {

        if (update(state, false)) {

            notifyStateChangeListeners();

        }
    }

    @Override
    public final String toString() {

        return getClass().getName() + ":" + getState().toString();

    }
}
