/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework;

/**
 * Methods common to groups of devices.
 *
 * Note that {@link Device#update(DeviceState, boolean)} for a group passes the
 * given {@link DeviceState} through to each of its members.
 *
 * Conversely, {@link Device#getState()} for a group must have plugin-specific
 * logic for combining or averaging the states of all its members.
 */
public interface Group extends Device {

    /**
     * Add the given {@link Device} to this group.
     *
     * @param member The new member.
     */
    void addMember(final Device member);

    /**
     * Get the members of this group.
     *
     * @return Every {@link Device} in this group.
     */
    Device[] getMembers();

    /**
     * Remove the given {@link Device} from this group.
     *
     * @param member The {@link Device} to remove.
     */
    void removeMember(final Device member);

}
