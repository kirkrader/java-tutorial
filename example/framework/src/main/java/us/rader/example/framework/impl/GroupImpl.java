/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import java.util.LinkedList;
import java.util.List;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;
import us.rader.example.framework.Group;
import us.rader.example.framework.Plugin;

/**
 * Super class of {@link Device} groups.
 */
public abstract class GroupImpl extends DeviceCommon
        implements Group {

    /**
     * Version constant required by {@link java.io.Serializable}.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The members of this group.
     */
    private final List<Device> members;

    /**
     *
     * /** Register group with given {@link Plugin}.
     *
     * @param plugin  The {@link Plugin} responsble for this group.
     *
     * @param id      The group's id.
     *
     * @param name    The group's name.
     *
     * @param members Initial set of members.
     *
     * @throws Exception If error encountered adding member.
     */
    public GroupImpl(final Plugin plugin, final String id,
            final String name, final Device... members)
            throws Exception {

        super(plugin, id, name);
        this.members = new LinkedList<>();

        for (final Device member : members) {

            addMember(member);

        }

        getPlugin().registerGroup(this);

    }

    @Override
    public synchronized final void addMember(final Device member) {

        if (member == null) {

            throw new NullPointerException("null member device");

        }

        if (members.contains(member)) {

            throw new IllegalArgumentException(
                    "duplicate group member " + member.toString());

        }

        members.add(member);
        memberStateChange(member);
        member.addStateChangeListener(this::memberStateChange);
        member.addClosingListener(this::memberClosing);

    }

    @Override
    public synchronized final void close() throws Exception {

        try {
            synchronized (members) {

                while (!members.isEmpty()) {

                    removeMember(members.get(0));

                }
            }

            getPlugin().unregisterGroup(this);

        } finally {

            super.close();

        }
    }

    @Override
    public final Device[] getMembers() {

        synchronized (members) {

            return members.toArray(new Device[members.size()]);

        }
    }

    @Override
    public synchronized DeviceState getState() {

        final DeviceState state = super.getState();

        boolean onPresent = false;
        boolean on = false;
        float totalLevel = 0.0f;
        int levelCount = 0;
        int totalColorTemperature = 0;
        int colorTemperatureCount = 0;
        float totalHue = 0.0f;
        int hueCount = 0;
        float totalSaturation = 0.0f;
        int saturationCount = 0;

        for (final Device member : members) {

            final DeviceState memberState = member.getState();

            final Boolean onValue = (Boolean) memberState.get("on");
            final Float levelValue = (Float) memberState.get("level");
            final Integer colorTemperatureValue = (Integer) memberState
                    .get("colorTemperature");
            final Float hueValue = (Float) memberState.get("hue");
            final Float saturationValue = (Float) memberState
                    .get("saturation");

            if (onValue != null) {

                on = on || onValue.booleanValue();
                onPresent = true;

            }

            if (levelValue != null) {

                totalLevel += levelValue;
                levelCount += 1;

            }

            if (colorTemperatureValue != null) {

                totalColorTemperature += colorTemperatureValue;
                colorTemperatureCount += 1;

            }

            if (hueValue != null) {

                totalHue += hueValue;
                hueCount += 1;

            }

            if (saturationValue != null) {

                totalSaturation += saturationValue;
                saturationCount += 1;

            }
        }

        if (onPresent) {

            state.put("on", on);

        }

        if (levelCount > 0) {

            state.put("level", totalLevel / levelCount);

        }

        if (colorTemperatureCount > 0) {

            state.put("colorTemperature",
                    totalColorTemperature / colorTemperatureCount);
        }

        if (hueCount > 0) {

            state.put("hue", totalHue / hueCount);

        }

        if (saturationCount > 0) {

            state.put("saturation",
                    totalSaturation / saturationCount);

        }

        return state;

    }

    /**
     * Remove members when they are closed.
     *
     * @param member The member that is closing.
     */
    protected void memberClosing(final Device member) {

        removeMember(member);

    }

    /**
     * Handle state-change events from member devices.
     *
     * @param member The member device.
     */
    protected synchronized void memberStateChange(
            final Device member) {

        notifyStateChangeListeners();

    }

    @Override
    public synchronized final void removeMember(final Device member) {

        member.removeStateChangeListener(this::memberStateChange);
        member.removeClosingListener(this::memberClosing);
        members.remove(member);
        memberStateChange(null);

    }

    @Override
    public boolean update(final DeviceState state,
            final boolean changed) {

        boolean stateChanged = changed;

        for (final Device member : members) {

            stateChanged = member.update(state, stateChanged);

        }

        return stateChanged;

    }
}
