/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework;

import java.util.function.Consumer;

import us.rader.example.framework.impl.SceneKey;

/**
 * Maintain collections of devices, groups and scenes.
 *
 * Handle state-change, name-change and closing callbacks.
 */
public interface DeviceListener extends AutoCloseable {

    /**
     * Handle {@link Device} closing event.
     *
     * @param sender The {@link Device}.
     */
    void deviceClosing(Device sender);

    /**
     * Handle {@link Device} name-change-event.
     *
     * @param sender  The {@link Device}.
     *
     * @param newName The new name.
     */
    void deviceNameChange(Device sender, String newName);

    /**
     * Handle {@link Device} state-change event.
     *
     * @param sender The {@link Device}.
     */
    void deviceStateChange(Device sender);

    /**
     * Get a registered {@link Device}.
     *
     * @param id The {@link Device} id.
     *
     * @return The specified {@link Device} or {@code null}.
     *
     * @see Device#getId()
     * @see #registerDevice(Device)
     * @see #unregisterDevice(Device)
     * @see #getDevices()
     */
    Device getDevice(String id);

    /**
     * Get every registered {@link Device}.
     *
     * @return Every registered {@link Device}.
     *
     * @see #getDevice(String)
     * @see #registerDevice(Device)
     * @see #unregisterDevice(Device)
     */
    Device[] getDevices();

    /**
     * Get a registered {@link Group}.
     *
     * @param id The {@link Group} id.
     *
     * @return The specified {@link Group} or {@code null}.
     *
     * @see Group#getId()
     * @see #registerGroup(Group)
     * @see #unregisterGroup(Group)
     * @see #getGroups()
     */
    Group getGroup(String id);

    /**
     * Get every registered {@link Group}.
     *
     * @return Every registered {@link Group}.
     *
     * @see #getGroup(String)
     * @see #registerGroup(Group)
     * @see #unregisterGroup(Group)
     */
    Group[] getGroups();

    /**
     * Get a registered {@link Scene}.
     *
     * @param key The unique identifier of the {@link Scene}.
     *
     * @return The specified {@link Scene} or {@code null}.
     *
     * @see #getScene(String, String)
     * @see #registerScene(Scene)
     * @see #unregisterDevice(Device)
     * @see #getScenes()
     */
    Scene getScene(SceneKey key);

    /**
     * Get the specified {@link Scene}.
     *
     * @param sceneId The {@link Scene} id.
     *
     * @param groupId The {@link Group} id.
     *
     * @return The specified {@link Scene} or {@code null}.
     *
     * @see #getScene(SceneKey)
     * @see #registerScene(Scene)
     * @see #getScenes()
     */
    Scene getScene(String sceneId, String groupId);

    /**
     * Get an array containing every registered {@link Scene}.
     *
     * @return Every registered {@link Scene}.
     *
     * @see #getScene(SceneKey)
     * @see #getScene(String, String)
     * @see #registerScene(Scene)
     */
    Scene[] getScenes();

    /**
     * Handle {@link Group} closing events.
     *
     * @param sender The {@link Group}.
     */
    void groupClosing(Device sender);

    /**
     * Handle {@link Group} name-change events.
     *
     * @param sender  The {@link Group}.
     *
     * @param newName The new name.
     */
    void groupNameChange(Device sender, String newName);

    /**
     * Handle {@link Group} state-change events.
     *
     * @param sender The {@link Group}.
     */
    void groupStateChange(Device sender);

    /**
     * Track the state of the given {@link Device}.
     *
     * @param device The {@link Device} to track.
     *
     * @see Device#addStateChangeListener(Consumer)
     */
    void registerDevice(Device device);

    /**
     * Track the state of the given {@link Group}.
     *
     * @param group The {@link Group} to track.
     *
     * @see Group#addStateChangeListener(Consumer)
     */
    void registerGroup(Group group);

    /**
     * Register the given {@link Scene}.
     *
     * @param scene The {@link Scene} to register.
     */
    void registerScene(Scene scene);

    /**
     * Handle {@link Scene} closing events.
     *
     * @param sender The {@link Scene}.
     */
    void sceneClosing(Device sender);

    /**
     * Handle {@link Scene} name-change events.
     *
     * @param sender  The {@link Scene}.
     *
     * @param newName The new name.
     */
    void sceneNameChange(Device sender, String newName);

    /**
     * Handle {@link Scene} state-change events.
     *
     * Note: this actually indicates that {@link Scene#activate()} was called, so
     * unconditionally turns on the {@link Group}.
     *
     * @param sender The {@link Scene}.
     */
    void sceneStateChange(Device sender);

    /**
     * Remove the given {@link Device}.
     *
     * @param device The {@link Device}.
     *
     * @return The removed {@link Device} or {@code null}.
     */
    Device unregisterDevice(Device device);

    /**
     * Remove the given {@link Group}.
     *
     * @param group The {@link Group}.
     *
     * @return The removed {@link Group} or {@code null}.
     */
    Group unregisterGroup(Group group);

    /**
     * Remove the given {@link Scene}.
     *
     * @param scene The {@link Scene}.
     *
     * @return The removed {@link Scene} or {@code null}.
     */
    Scene unregisterScene(Scene scene);

}
