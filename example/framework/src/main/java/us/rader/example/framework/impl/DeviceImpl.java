/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import us.rader.example.framework.Device;
import us.rader.example.framework.Plugin;

/**
 * Super class of all home automation devices.
 */
public abstract class DeviceImpl extends DeviceCommon
        implements Device {

    /**
     * Version constant required by {@link java.io.Serializable}.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Register device with the given {@link Plugin}.
     *
     * @param plugin The {@link Plugin} responsible for this device.
     *
     * @param id     The device's id.
     *
     * @param name   The device's name.
     */
    public DeviceImpl(final Plugin plugin, final String id,
            final String name) {

        super(plugin, id, name);
        getPlugin().registerDevice(this);

    }

    /**
     * Remove this Device from the inversion of control framework.
     */
    @Override
    public synchronized void close() throws Exception {

        try {

            getPlugin().unregisterDevice(this);

        } finally {

            super.close();

        }
    }
}
