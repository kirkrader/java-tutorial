/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import static java.util.logging.Level.WARNING;

import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import us.rader.example.framework.Framework;
import us.rader.example.framework.Plugin;

/**
 * Super class of objects that implement {@link Framework}.
 */
public abstract class FrameworkImpl extends DeviceListenerImpl
        implements Framework {

    /**
     * {@link Logger} used for debugging.
     */
    private static final Logger logger;

    static {

        logger = Logger.getLogger(FrameworkImpl.class.getName());

    }

    /**
     * All loaded plugins.
     */
    private final List<Plugin> plugins;

    /**
     * Default constructor.
     */
    public FrameworkImpl() {

        plugins = new LinkedList<>();

    }

    @Override
    public final Plugin addIfPlugin(final Class<?> cls)
            throws ReflectiveOperationException {

        if (!Plugin.class.isAssignableFrom(cls)) {

            return null;

        }

        final Constructor<?> constructor = cls
                .getConstructor(Framework.class);
        return (Plugin) constructor.newInstance(this);

    }

    @Override
    public final void addPlugin(final Plugin plugin) {

        if (plugin == null) {

            throw new NullPointerException("null plugin");

        }

        synchronized (plugins) {

            if (plugins.contains(plugin)) {

                throw new IllegalArgumentException(
                        "duplicate plugin " + plugin);

            }

            plugins.add(plugin);

        }
    }

    @Override
    public void close() throws Exception {

        try {
            synchronized (plugins) {

                final Iterator<Plugin> iterator = plugins.iterator();

                while (iterator.hasNext()) {

                    try (final Plugin plugin = iterator.next()) {

                        iterator.remove();

                    } catch (final Exception e) {

                        if (logger.isLoggable(WARNING)) {

                            logger.log(WARNING, e.getMessage(), e);

                        }
                    }
                }
            }

        } finally {

            super.close();

        }
    }

    @Override
    public final Plugin[] getPlugins() {

        return plugins.toArray(new Plugin[plugins.size()]);

    }

    @Override
    public final void removePlugin(final Plugin plugin) {

        synchronized (plugins) {

            plugins.remove(plugin);

        }
    }
}
