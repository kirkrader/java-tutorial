/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework;

/**
 * Inversion of control framework methods.
 */
public interface Framework extends DeviceListener {

    /**
     * Create an instance of the given class and add it to the given list if it is a
     * plugin type.
     *
     * 1. Extends {@link java.lang.AutoCloseable}
     *
     * 2. Is annotated with {@link Plugin}
     *
     * @param cls The class to check.
     *
     * @return The newly created {@link Plugin} or {@code null}.
     *
     * @throws ReflectiveOperationException Required by declaration of reflective
     *                                      operations.
     */
    Plugin addIfPlugin(Class<?> cls)
            throws ReflectiveOperationException;

    /**
     * Add the given {@link Plugin}.
     *
     * @param plugin The plugin to add.
     *
     * @see #removePlugin(Plugin)
     */
    void addPlugin(Plugin plugin);

    /**
     * Get every registered {@link Plugin},
     *
     * @return Every registered {@link Plugin},
     */
    Plugin[] getPlugins();

    /**
     * Remove the given {@link Plugin}.
     *
     * @param plugin The {@link Plugin}.
     *
     * @see #addPlugin(Plugin)
     */
    void removePlugin(Plugin plugin);

}
