/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import us.rader.example.framework.Device;
import us.rader.example.framework.Framework;
import us.rader.example.framework.Group;
import us.rader.example.framework.Plugin;
import us.rader.example.framework.Scene;

/**
 * Super-class of {@link Plugin} objects.
 */
public abstract class PluginImpl extends DeviceListenerImpl
        implements Plugin {

    /**
     * The active {@link Framework}.
     */
    private final Framework framework;

    /**
     * Initialize {@link Device}, {@link Group} and {@link Scene} maps.
     *
     * @param framework The active {@link Framework}.
     */
    public PluginImpl(final Framework framework) {

        this.framework = framework;
        this.framework.addPlugin(this);

    }

    @Override
    public void close() throws Exception {

        try {

            framework.removePlugin(this);

        } finally {

            super.close();

        }
    }

    @Override
    public Framework getFramework() {

        return framework;

    }

    @Override
    public void registerDevice(final Device device) {

        super.registerDevice(device);
        framework.registerDevice(device);

    }

    @Override
    public void registerGroup(final Group group) {

        super.registerGroup(group);
        framework.registerGroup(group);

    }

    @Override
    public void registerScene(final Scene scene) {

        super.registerScene(scene);
        framework.registerScene(scene);

    }

    @Override
    public Device unregisterDevice(final Device device) {

        framework.unregisterDevice(device);
        return super.unregisterDevice(device);

    }

    @Override
    public Group unregisterGroup(final Group group) {

        framework.unregisterGroup(group);
        return super.unregisterGroup(group);

    }

    @Override
    public Scene unregisterScene(final Scene scene) {

        framework.unregisterScene(scene);
        return super.unregisterScene(scene);

    }
}
