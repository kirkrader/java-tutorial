/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework;

import java.io.Serializable;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import us.rader.example.framework.impl.DeviceImpl;

/**
 * Methods common to all home automation devices
 *
 * @see DeviceImpl
 */
public interface Device extends AutoCloseable, Serializable {

    /**
     * Add a closing listener.
     *
     * @param callback Function to call when this device's state changes.
     *
     *
     */
    void addClosingListener(Consumer<Device> callback);

    /**
     * Add a name-change listener.
     *
     * @param callback Function to call when this device's state changes.
     *
     *
     */
    void addNameChangeListener(BiConsumer<Device, String> callback);

    /**
     * Add a state-change listener.
     *
     * @param callback Function to call when this device's state changes.
     */
    void addStateChangeListener(Consumer<Device> callback);

    /**
     * Get the immutable device id.
     *
     * @return The device id.
     */
    String getId();

    /**
     * The device's mutable name string.
     *
     * @return The name.
     *
     * @see #getName()
     * @see #setName(String)
     */
    String getName();

    /**
     * Return the {@link Plugin} responsible for this device.
     *
     * @return The {@link Plugin}.
     */
    Plugin getPlugin();

    /**
     * Get state-change event data for this device.
     *
     * @return {@link DeviceState} for this instance.
     */
    DeviceState getState();

    /**
     * Remove a closing listener
     *
     * @param callback Function to stop calling when this device's state changes.
     */
    void removeClosingListener(final Consumer<Device> callback);

    /**
     * Remove a name-change listener
     *
     * @param callback Function to stop calling when this device's state changes.
     */
    void removeNameChangeListener(
            final BiConsumer<Device, String> callback);

    /**
     * Remove a state-change listener
     *
     * @param callback Function to stop calling when this device's state changes.
     */
    void removeStateChangeListener(final Consumer<Device> callback);

    /**
     * Change the device name.
     *
     * @param name The new name.
     *
     * @see #getName()
     */
    void setName(String name);

    /**
     * Update state of this device.
     *
     * Invokes {@link #update(DeviceState, boolean)} and fires a state-change event
     * if it returns {@code true}.
     *
     * Normally, you should not override this method but
     * {@link #update(DeviceState, boolean)}, instead.
     *
     * @param state Descriptor for the new state.
     *
     * @see #addStateChangeListener(Consumer)
     * @see #update(DeviceState, boolean)
     */
    void setState(DeviceState state);

    /**
     * Plugin-specific handler for {@link #setState(DeviceState)}.
     *
     * Override this to support plugin-specific features for devices.
     *
     * Normally, you should not call this directly but
     * {@link #setState(DeviceState)}, instead. If you do call this and it returns
     * {@code true} then you should arrange to notify state-change listeners.
     *
     * Note: in a more competent object-oriented programming language than Java,
     * this method would not be public at all.
     *
     * @param state   Descriptor for new state.
     *
     * @param changed Force state-change event.
     *
     * @return {@code true} if a state-change event should be fired.
     *
     * @see #setState(DeviceState)
     */
    boolean update(DeviceState state, boolean changed);

}
