/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import us.rader.example.framework.Scene;

/**
 * Scene and group id pair.
 *
 * Unique id for objects that implement {@link Scene}.
 */
public final class SceneKey {

    /**
     * Value to match with {@link Scene#getGroupId()}.
     */
    public final String groupId;

    /**
     * Value to match with {@link Scene#getId()}.
     */
    public final String sceneId;

    /**
     * Validate and initialize {@link #sceneId} and {@link #groupId}.
     *
     * @param sceneId Value for {@link #sceneId}}.
     *
     * @param groupId Value for {@link #groupId}.
     */
    public SceneKey(final String sceneId, final String groupId) {

        if (sceneId == null) {
            throw new NullPointerException("null scene id");
        }

        if (groupId == null) {
            throw new NullPointerException("null group id");
        }

        this.sceneId = sceneId;
        this.groupId = groupId;

    }

    @Override
    public boolean equals(final Object obj) {

        if (obj == null) {

            return false;

        }

        if (!(obj instanceof SceneKey)) {

            return false;

        }

        return hashCode() == obj.hashCode();

    }

    @Override
    public int hashCode() {

        return sceneId.hashCode() + (3 * groupId.hashCode());

    }

    @Override
    public String toString() {

        return "{" + sceneId + "," + groupId + "}";

    }
}
