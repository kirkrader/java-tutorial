/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import java.util.logging.Logger;

/**
 * Global constants and functions.
 */
public final class Utilities {

    /**
     * {@link Logger} for debugging.
     */
    static final Logger logger;

    /**
     * Constant used in mired calculations.
     */
    private static final double MK;

    static {

        logger = Logger.getLogger(Utilities.class.getName());
        MK = 1000000.0d;

    }

    /**
     * Convert color temperature in Kelvins to the corresponding mired value.
     *
     * @param kelvins Color temperature in degrees Kelvin.
     *
     * @return The equivalent number of mireds.
     */
    public static int kelvinsToMireds(final int kelvins) {

        return (int) Math.floor(MK / kelvins);

    }

    /**
     * Convert color temperature in mireds to corresponding Kelvins value.
     *
     * @param mireds Color temperature in mireds.
     *
     * @return The equivalent number of degrees Kelvin.
     */
    public static int miredsToKelvins(final int mireds) {

        return (int) Math.floor(MK * (1.0d / mireds));

    }

    /**
     * Compare two floats for "equality."
     *
     * Returns {@code true} if and only if the magnitude of the difference between
     * the two parameters is less than the magnitude of
     * {@link java.lang.Math#ulp(float)}.
     *
     * @param f1 A float.
     *
     * @param f2 Another float.
     *
     * @return {@code true} iff the difference between the floats is less than
     *         detectable given the precision of IEEE floating-point numbers.
     */
    public static boolean sameFloat(final float f1, final float f2) {

        return Math.abs(f1 - f2) < Math
                .ulp(Math.min(Math.abs(f1), Math.abs(f2)));

    }

    /**
     * Hide utility class' constructor.
     */
    private Utilities() {

        // nothing to do here

    }
}
