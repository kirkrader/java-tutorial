/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import us.rader.example.framework.Group;
import us.rader.example.framework.Plugin;
import us.rader.example.framework.Scene;

/**
 * Super class of objects that implement {@link Scene}.
 */
public abstract class SceneImpl extends DeviceCommon
        implements Scene {

    /**
     * Version constant required by {@link java.io.Serializable}.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Group id.
     *
     * @see #getGroupId()
     */
    private final String groupId;

    /**
     * Initialize required fields.
     *
     * @param plugin  The {@link Plugin} responseible for this scene.
     *
     * @param id      Scene id.
     *
     * @param name    Scene name.
     *
     * @param groupId {@link Group} id.
     */
    public SceneImpl(final Plugin plugin, final String id,
            final String name, final String groupId) {

        super(plugin, id, name);

        if (groupId == null) {

            throw new NullPointerException("null groupId");

        }

        this.groupId = groupId;
        getPlugin().registerScene(this);

    }

    @Override
    public synchronized void activate() throws Exception {

        notifyStateChangeListeners();

    }

    @Override
    public synchronized final void close() throws Exception {

        try {

            getPlugin().unregisterScene(this);

        } finally {

            super.close();

        }
    }

    @Override
    public synchronized final String getGroupId() {

        return groupId;

    }
}
