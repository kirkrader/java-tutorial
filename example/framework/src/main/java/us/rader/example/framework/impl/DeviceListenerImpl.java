/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.framework.impl;

import static java.util.logging.Level.WARNING;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.logging.Logger;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceListener;
import us.rader.example.framework.Group;
import us.rader.example.framework.Scene;

/**
 * Super-class of {@link DeviceListener} objects.
 */
public abstract class DeviceListenerImpl implements DeviceListener {

    /**
     * {@link Logger} for debugging.
     */
    static final Logger logger;

    static {

        logger = Logger.getLogger(DeviceListenerImpl.class.getName());

    }

    /**
     * Close all the given {@link AutoCloseable} objects.
     *
     * @param elements The {@link AutoCloseable} objects.
     */
    protected static void closeAll(final AutoCloseable... elements) {

        for (final AutoCloseable element : elements) {

            try {

                element.close();

            } catch (final Exception e) {

                if (logger.isLoggable(WARNING)) {

                    logger.log(WARNING, e.getMessage(), e);

                }
            }
        }
    }

    /**
     * Get the value from the given {@link Map} selected by the given key.
     *
     * @param <K> The type of keys in the given {@link Map}.
     *
     * @param <V> The type of values in the given {@link Map}.
     *
     * @param map The {@link Map}.
     *
     * @param key The key.
     *
     * @return The selected value or {@code null}.
     */
    protected static <K, V> V get(final Map<K, V> map, final K key) {

        synchronized (map) {

            return map.get(key);

        }
    }

    /**
     * Get all values in the given {@link Map}.
     *
     * @param <K>  The type of keys in the given {@link Map}.
     *
     * @param <V>  The type of values in the given {@link Map}.
     *
     * @param type The meta-type of {@code V}.
     *
     * @param map  The {@link Map}.
     *
     * @return All values in the given {@link Map}.
     */
    @SuppressWarnings("unchecked")
    protected static <K, V> V[] getAll(final Class<V> type,
            final Map<K, V> map) {

        synchronized (map) {

            return map.values().toArray(
                    (V[]) Array.newInstance(type, map.size()));

        }
    }

    /**
     * Add the given value to the given {@link Map}.
     *
     * @param map         The {@link Map}.
     *
     * @param key         The new entry's key.
     *
     * @param value       The new entry's value.
     *
     * @param closing     Closing callback.
     *
     * @param nameChange  Name-change callback.
     *
     * @param stateChange State-change callback.
     *
     * @param <K>         The type of keys in the given {@link Map}.
     *
     * @param <V>         The type of values in the given {@link Map}.
     */
    protected static <K, V extends Device> void put(
            final Map<K, V> map, final K key, final V value,
            final Consumer<Device> closing,
            final BiConsumer<Device, String> nameChange,
            final Consumer<Device> stateChange) {

        synchronized (map) {

            if (map.containsKey(key)) {

                throw new IllegalArgumentException("duplicate key");

            }

            map.put(key, value);
            value.addClosingListener(closing);
            value.addNameChangeListener(nameChange);
            value.addStateChangeListener(stateChange);

        }
    }

    /**
     * Remove the given value from the given map.
     *
     * @param <K>         The type of keys.
     *
     * @param <V>         The type of values.
     *
     * @param map         The map.
     *
     * @param key         The key.
     *
     * @param value       The value to remove.
     *
     * @param closing     The closing callback to unregister.
     *
     * @param nameChange  The name-change callback to unregister.
     *
     * @param stateChange The state-change callback to unregister.
     *
     * @return The removed value or {@code null}.
     */
    protected static <K, V extends Device> V remove(
            final Map<K, V> map, final K key, final V value,
            final Consumer<Device> closing,
            final BiConsumer<Device, String> nameChange,
            final Consumer<Device> stateChange) {

        synchronized (map) {

            value.removeClosingListener(closing);
            value.removeNameChangeListener(nameChange);
            value.removeStateChangeListener(stateChange);
            return map.remove(key);

        }
    }

    /**
     * Every registered {@link Device}.
     */
    private final Map<String, Device> devices;

    /**
     * Every registered {@link Group}.
     */
    private final Map<String, Group> groups;

    /**
     * Every registered {@link Scene}.
     */
    private final Map<SceneKey, Scene> scenes;

    /**
     * Initialize collections of {@link Group} and {@link Scene} objects.
     */
    public DeviceListenerImpl() {

        devices = new HashMap<>();
        groups = new HashMap<>();
        scenes = new HashMap<>();
    }

    @Override
    public void close() throws Exception {

        closeAll(getScenes());
        closeAll(getGroups());
        closeAll(getDevices());

    }

    @Override
    public void deviceClosing(final Device sender) {

        unregisterDevice(sender);

    }

    @Override
    public void deviceNameChange(final Device sender,
            final String newName) {

        // deliberately empty

    }

    @Override
    public void deviceStateChange(final Device sender) {

        // deliberately empty

    }

    @Override
    public final Device getDevice(final String id) {

        return get(devices, id);

    }

    @Override
    public final Device[] getDevices() {

        return getAll(Device.class, devices);

    }

    @Override
    public final Group getGroup(final String id) {

        return get(groups, id);

    }

    @Override
    public final Group[] getGroups() {

        return getAll(Group.class, groups);

    }

    @Override
    public final Scene getScene(final SceneKey key) {

        return get(scenes, key);

    }

    @Override
    public final Scene getScene(final String sceneId,
            final String groupId) {

        return getScene(new SceneKey(sceneId, groupId));

    }

    @Override
    public final Scene[] getScenes() {

        return getAll(Scene.class, scenes);

    }

    @Override
    public void groupClosing(final Device sender) {

        unregisterGroup((Group) sender);

    }

    @Override
    public void groupNameChange(final Device sender,
            final String newName) {

        // deliberately empty

    }

    @Override
    public void groupStateChange(final Device sender) {

        // deliberately empty

    }

    @Override
    public void registerDevice(final Device device) {

        put(devices, device.getId(), device, this::deviceClosing,
                this::deviceNameChange, this::deviceStateChange);

    }

    @Override
    public void registerGroup(final Group group) {

        put(groups, group.getId(), group, this::groupClosing,
                this::groupNameChange, this::groupStateChange);

    }

    @Override
    public void registerScene(final Scene scene) {

        put(scenes, new SceneKey(scene.getId(), scene.getGroupId()),
                scene, this::sceneClosing, this::sceneNameChange,
                this::sceneStateChange);

    }

    @Override
    public void sceneClosing(final Device sender) {

        unregisterScene((Scene) sender);

    }

    @Override
    public void sceneNameChange(final Device sender,
            final String newName) {

        // deliberately empty

    }

    @Override
    public void sceneStateChange(final Device sender) {

        // deliberately empty

    }

    @Override
    public Device unregisterDevice(final Device device) {

        return remove(devices, device.getId(), device,
                this::deviceClosing, this::deviceNameChange,
                this::deviceStateChange);

    }

    @Override
    public Group unregisterGroup(final Group group) {

        return remove(groups, group.getId(), group,
                this::groupClosing, this::groupNameChange,
                this::groupStateChange);

    }

    @Override
    public Scene unregisterScene(final Scene scene) {

        return remove(scenes,
                new SceneKey(scene.getId(), scene.getGroupId()),
                scene, this::sceneClosing, this::sceneNameChange,
                this::sceneStateChange);

    }
}
