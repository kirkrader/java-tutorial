/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.app;

import static java.util.logging.Level.FINER;
import static java.util.logging.Level.FINEST;
import static java.util.logging.Level.SEVERE;
import static java.util.logging.Level.WARNING;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

import us.rader.example.framework.Device;
import us.rader.example.framework.Framework;
import us.rader.example.framework.Group;
import us.rader.example.framework.Scene;
import us.rader.example.framework.impl.FrameworkImpl;

/**
 * Inversion of control framework for objects that implement the Device
 * interface.
 */
final class App extends FrameworkImpl {

    /**
     * {@link java.util.logging.Logger} used for debugging and analytics.
     */
    private static final Logger logger;

    static {

        logger = Logger.getLogger(App.class.getName());

    }

    /**
     * Placeholder implementation for all callbacks.
     *
     * @param label  Label string.
     *
     * @param sender Sender {@link Device}.
     *
     * @param args   Additional arguments.
     */
    private static void callback(final String label,
            final Device sender, final Object... args) {

        try {

            final StringBuilder builder = new StringBuilder(label);

            for (final Object arg : args) {

                builder.append(" ");
                builder.append(arg.toString());

            }

            builder.append(" ");
            builder.append(sender.toString());
            System.out.println(builder.toString());

        } catch (final Exception e) {

            if (logger.isLoggable(WARNING)) {

                logger.log(WARNING, e.getMessage(), e);

            }
        }
    }

    /**
     * Application process entry point.
     *
     * @param args Command-line arguments.
     */
    public static void main(final String[] args) {

        try (final App app = new App()) {

            System.out.println();
            System.out.println("Devices:");

            for (final Device device : app.getDevices()) {

                System.out.println(device);

            }

            System.out.println();
            System.out.println("Groups:");

            for (final Group group : app.getGroups()) {

                System.out.println(group);

            }

            System.out.println();
            System.out.println("Scenes:");

            for (final Scene scene : app.getScenes()) {

                System.out.println(scene);

            }

            System.out.println("Press <enter> to exit...");
            System.in.read();

        } catch (final Exception e) {

            logger.log(SEVERE, e.getMessage(), e);

        }
    }

    /**
     * Load plugins dynamically.
     */
    public App() {

        loadPlugins();

    }

    @Override
    public void deviceClosing(final Device sender) {

        callback("deviceClosing", sender);

    }

    @Override
    public void deviceNameChange(final Device sender,
            final String newName) {

        callback("deviceNameChange", sender, newName);

    }

    @Override
    public void deviceStateChange(final Device sender) {

        callback("deviceStateChange", sender);

    }

    @Override
    public void groupClosing(final Device sender) {

        callback("groupCLosing", sender);

    }

    @Override
    public void groupNameChange(final Device sender,
            final String newName) {

        callback("groupNameChange", sender, newName);

    }

    @Override
    public void groupStateChange(final Device sender) {

        callback("groupStateChange", sender);

    }

    /**
     * Call {@link Framework#addIfPlugin(Class)} for each class in the given
     * {@link java.util.jar.JarFile}.
     *
     * @param jarFile     The {@link java.util.jar.JarFile} from which to load
     *                    classes.
     *
     * @param classLoader The {@link java.lang.ClassLoader} to use.
     *
     * @throws ReflectiveOperationException Required by declaration of reflective
     *                                      operations.
     *
     * @see Framework#addIfPlugin(Class)
     */
    private void loadClasses(final JarFile jarFile,
            final ClassLoader classLoader)
            throws ReflectiveOperationException {

        if (logger.isLoggable(FINER)) {
            logger.log(FINER,
                    "loading plugins from " + jarFile.getName());
        }

        final Enumeration<JarEntry> entries = jarFile.entries();

        while (entries.hasMoreElements()) {

            final JarEntry entry = entries.nextElement();
            final String name = entry.getName();

            if (logger.isLoggable(FINEST)) {
                logger.log(FINEST, "checking " + name);
            }

            if (name.endsWith(".class")) {

                final Class<?> cls = classLoader
                        .loadClass(name.replace('/', '.').substring(0,
                                name.length() - 6));
                addIfPlugin(cls);

            }
        }
    }

    /**
     * Load device handler plugins.
     *
     * Call {@link #loadClasses(JarFile, ClassLoader)} for each JAR file in the
     * directory specified by the "us.rader.example.plugins" system property.
     *
     * @see #loadClasses(JarFile, ClassLoader)
     */
    private void loadPlugins() {

        try {

            final String pluginsPath = System
                    .getProperty("us.rader.example.plugins");

            if (pluginsPath == null) {

                if (logger.isLoggable(WARNING)) {

                    logger.log(WARNING,
                            "us.rader.example.plugins system property not defined");

                }

                return;

            }

            final File pluginsDir = new File(pluginsPath);

            if (logger.isLoggable(FINER)) {
                logger.log(FINER, "loading plugins from "
                        + pluginsDir.getCanonicalPath());
            }

            for (final File pluginJar : pluginsDir.listFiles(
                    (dir, name) -> name.endsWith(".jar"))) {

                final String path = pluginJar.getCanonicalPath();
                final URL[] urls = {
                        new URL("jar:file:" + path + "!/") };
                final URLClassLoader classLoader = URLClassLoader
                        .newInstance(urls,
                                FrameworkImpl.class.getClassLoader());

                try (final JarFile jarFile = new JarFile(pluginJar)) {
                    loadClasses(jarFile, classLoader);
                }
            }

        } catch (final Exception e) {

            logger.log(SEVERE, "error loading plugin", e);

        }
    }

    @Override
    public void sceneClosing(final Device sender) {

        callback("sceneClosing", sender);

    }

    @Override
    public void sceneNameChange(final Device sender,
            final String newName) {

        callback("sceneNameChange", sender);

    }

    @Override
    public void sceneStateChange(final Device sender) {

        callback("sceneStateChange", sender);

    }
}
