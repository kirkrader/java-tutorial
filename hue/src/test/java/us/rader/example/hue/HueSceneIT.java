/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.lang.reflect.Field;
import java.util.Map;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import us.rader.example.framework.impl.SceneKey;

@SuppressWarnings("javadoc")
@Tag("integration-test")
public class HueSceneIT {

    @Test
    public void activate() throws Exception {

        try (final MockFramework framework = new MockFramework()) {

            try (final HueBridge bridge = new HueBridge(framework)) {

                final Field scenesField = HueBridge.class
                        .getDeclaredField("scenes");
                assertNotNull(scenesField);
                scenesField.setAccessible(true);
                @SuppressWarnings("unchecked")
                final Map<SceneKey, HueScene> scenes = (Map<SceneKey, HueScene>) scenesField
                        .get(bridge);
                assertNotNull(scenes);
                final HueScene scene = scenes.get(new SceneKey(
                        System.getProperty(
                                "us.rader.example.hue.test.scene"),
                        System.getProperty(
                                "us.rader.example.hue.test.group")));
                assertNotNull(scene);
                scene.activate();

            }
        }
    }
}