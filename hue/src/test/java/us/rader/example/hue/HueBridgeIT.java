/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.function.Function;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import us.rader.example.framework.DeviceState;

@SuppressWarnings("javadoc")
@Tag("integration-test")
public class HueBridgeIT {

    public static void waitForEvent(final DeviceState[] event,
            final Function<DeviceState, Boolean> test)
            throws InterruptedException {

        int retries = 40;

        while ((event[0] == null) || !test.apply(event[0])) {

            if (retries-- < 0) {
                throw new IllegalStateException(
                        "timeout waiting for event");
            }

            Thread.sleep(100);

        }
    }

    @Test
    public void sendGet() throws Exception {

        assertNotNull(
                System.getProperty("us.rader.example.hue.address"));
        assertNotNull(
                System.getProperty("us.rader.example.hue.token"));

        try (final MockFramework framework = new MockFramework()) {

            try (final HueBridge bridge = new HueBridge(framework)) {

                final JsonNode response = bridge.sendGet();
                assertNotNull(response.required("lights"));

            }
        }
    }

    @Test
    public void sendPut() throws Exception {

        assertNotNull(
                System.getProperty("us.rader.example.hue.address"));
        assertNotNull(
                System.getProperty("us.rader.example.hue.token"));

        try (final MockFramework framework = new MockFramework()) {

            try (final HueBridge bridge = new HueBridge(framework)) {

                final String id = System.getProperty(
                        "us.rader.example.hue.test.light");
                assertNotNull(id);
                final Field lightsField = HueBridge.class
                        .getDeclaredField("lights");
                lightsField.setAccessible(true);
                @SuppressWarnings("unchecked")
                final Map<String, HueDevice> lights = (Map<String, HueDevice>) lightsField
                        .get(bridge);
                final HueDevice device = lights.get(id);
                final DeviceState[] received = new DeviceState[1];
                received[0] = null;
                device.addStateChangeListener(
                        light -> received[0] = light.getState());
                final JsonNode response = bridge.sendGet("lights",
                        id);

                if (response instanceof ArrayNode) {

                    System.err.println(new ObjectMapper()
                            .writerWithDefaultPrettyPrinter()
                            .writeValueAsString(response));
                    fail("error response from HueBridge.sendGet()");

                }

                final DeviceState request = new DeviceState();

                if (response.required("state").required("on")
                        .asBoolean()) {

                    request.put("on", false);
                    HueBridge.checkResponse(bridge.sendPut(request,
                            "lights", id, "state"));
                    waitForEvent(received,
                            event -> !(Boolean) event.get("on"));

                }

                request.put("on", true);
                HueBridge.checkResponse(bridge.sendPut(request,
                        "lights", id, "state"));
                waitForEvent(received,
                        event -> (boolean) event.get("on"));
                Thread.sleep(1000);
                request.put("on", false);
                HueBridge.checkResponse(bridge.sendPut(request,
                        "lights", id, "state"));
                waitForEvent(received,
                        event -> !(boolean) event.get("on"));

            }
        }
    }
}
