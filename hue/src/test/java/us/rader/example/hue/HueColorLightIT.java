/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.lang.reflect.Field;
import java.util.Map;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import us.rader.example.framework.DeviceState;
import us.rader.example.framework.impl.Utilities;

@SuppressWarnings("javadoc")
@Tag("integration-test")
public class HueColorLightIT {

    @Test
    public void setColorableState() throws Exception {

        assertNotNull(
                System.getProperty("us.rader.example.hue.address"));
        assertNotNull(
                System.getProperty("us.rader.example.hue.token"));

        try (final MockFramework framework = new MockFramework()) {

            try (final HueBridge bridge = new HueBridge(framework)) {

                final String id = System.getProperty(
                        "us.rader.example.hue.test.light");
                assertNotNull(id);
                final Field lightsField = HueBridge.class
                        .getDeclaredField("lights");
                lightsField.setAccessible(true);
                @SuppressWarnings("unchecked")
                final Map<String, HueDevice> lights = (Map<String, HueDevice>) lightsField
                        .get(bridge);
                final HueDevice device = lights.get(id);

                if (device instanceof HueColorLight) {

                    final HueColorLight colorLight = (HueColorLight) device;
                    assertNotNull(colorLight);
                    final DeviceState[] received = new DeviceState[1];
                    colorLight.addStateChangeListener(
                            light -> received[0] = light.getState());

                    if (colorLight.isOn()) {

                        colorLight.setSwitchableState(false);
                        HueBridgeIT.waitForEvent(received,
                                event -> !(boolean) event.get("on"));

                    }

                    colorLight.setColorableState(true, 1.0f, 0.5f,
                            1.0f);
                    HueBridgeIT.waitForEvent(received,
                            event -> (boolean) event.get("on"));
                    Thread.sleep(1000);
                    colorLight.setSwitchableState(false);
                    HueBridgeIT.waitForEvent(received,
                            event -> !(boolean) event.get("on"));

                }
            }
        }
    }

    @Test
    public void setDimmableState() throws Exception {

        assertNotNull(
                System.getProperty("us.rader.example.hue.address"));
        assertNotNull(
                System.getProperty("us.rader.example.hue.token"));

        try (final MockFramework framework = new MockFramework()) {

            try (final HueBridge bridge = new HueBridge(framework)) {

                final String id = System.getProperty(
                        "us.rader.example.hue.test.light");
                assertNotNull(id);
                final Field lightsField = HueBridge.class
                        .getDeclaredField("lights");
                lightsField.setAccessible(true);
                @SuppressWarnings("unchecked")
                final Map<String, HueDevice> lights = (Map<String, HueDevice>) lightsField
                        .get(bridge);
                final HueDevice device = lights.get(id);

                if (device instanceof HueColorLight) {

                    final HueColorLight colorLight = (HueColorLight) device;
                    assertNotNull(colorLight);
                    final DeviceState[] received = new DeviceState[1];
                    colorLight.addStateChangeListener(
                            light -> received[0] = light.getState());

                    if (colorLight.isOn()) {

                        colorLight.setSwitchableState(false);
                        HueBridgeIT.waitForEvent(received,
                                event -> !(boolean) event.get("on"));

                    }

                    colorLight.setDimmableState(true, 0.5f);
                    HueBridgeIT.waitForEvent(received,
                            event -> (boolean) event.get("on"));
                    Thread.sleep(1000);
                    colorLight.setSwitchableState(false);
                    HueBridgeIT.waitForEvent(received,
                            event -> !(boolean) event.get("on"));

                }
            }
        }
    }

    @Test
    public void setMiredTunableState() throws Exception {

        assertNotNull(
                System.getProperty("us.rader.example.hue.address"));
        assertNotNull(
                System.getProperty("us.rader.example.hue.token"));

        try (final MockFramework framework = new MockFramework()) {

            try (final HueBridge bridge = new HueBridge(framework)) {

                final String id = System.getProperty(
                        "us.rader.example.hue.test.light");
                assertNotNull(id);
                final Field lightsField = HueBridge.class
                        .getDeclaredField("lights");
                lightsField.setAccessible(true);
                @SuppressWarnings("unchecked")
                final Map<String, HueDevice> lights = (Map<String, HueDevice>) lightsField
                        .get(bridge);
                final HueDevice device = lights.get(id);

                if (device instanceof HueColorLight) {

                    final HueColorLight colorLight = (HueColorLight) device;
                    assertNotNull(colorLight);
                    final DeviceState[] received = new DeviceState[1];
                    colorLight.addStateChangeListener(
                            light -> received[0] = light.getState());

                    if (colorLight.isOn()) {

                        colorLight.setSwitchableState(false);
                        HueBridgeIT.waitForEvent(received,
                                event -> !(boolean) event.get("on"));

                    }

                    colorLight.setMiredTunableState(true, 1.0f,
                            Utilities.kelvinsToMireds(25000));
                    HueBridgeIT.waitForEvent(received,
                            event -> (boolean) event.get("on"));
                    Thread.sleep(1000);
                    colorLight.setSwitchableState(false);
                    HueBridgeIT.waitForEvent(received,
                            event -> !(boolean) event.get("on"));

                }
            }
        }
    }
}