/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@SuppressWarnings("javadoc")
@Tag("unit-test")
public class HueGroupTypeUT {

    @Test
    public void color() {
        final HueGroupType type = HueGroupType.valueOfType("Room");
        assertSame(HueGroupType.ROOM, type);
    }

    @Test
    public void other() {
        try {
            HueGroupType.valueOfType(" ");
            fail("IllegalArgumentException expected");
        } catch (final IllegalArgumentException e) {

            // exception expected, test passed

        }
    }

    @Test
    public void plug() {
        final HueGroupType type = HueGroupType.valueOfType("Zone");
        assertSame(HueGroupType.ZONE, type);
    }
}
