/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;
import us.rader.example.framework.impl.Utilities;

/**
 * Wrapper for Philips Hue "Dimmable light" devices.
 *
 * @see HueLightType#DIMMABLE
 */
public class HueDimmableLight extends HuePlug {

    /**
     * {@link Logger} for debugging.
     */
    private static final Logger logger;

    /**
     * Version constant required by {@link java.io.Serializable}.
     */
    private static final long serialVersionUID;

    static {

        serialVersionUID = 1L;
        logger = Logger.getLogger(HueDimmableLight.class.getName());

    }

    /**
     * The dimmer level.
     */
    private float level;

    /**
     * Initialize {@link #getBridge()} in addition to
     * {@link us.rader.example.framework.impl.DeviceImpl} parameters.
     *
     * @param bridge The {@link HueBridge} that created this instance.
     *
     * @param id     Value for {@link Device#getId()}
     *
     * @param name   Value for {@link Device#getName()}
     *
     * @throws Exception Required by super class declaration.
     */
    public HueDimmableLight(final HueBridge bridge, final String id,
            final String name) throws Exception {

        super(bridge, id, name);

    }

    /**
     * Get the dimmer level.
     *
     * @return {@link #level}
     */
    public final synchronized float getLevel() {

        return level;

    }

    @Override
    public synchronized DeviceState getState() {

        final DeviceState state = super.getState();
        state.put("level", level);
        return state;

    }

    /**
     * Send PUT request to change the state of this light.
     *
     * @param on    New value for {@link HuePlug#isOn()}.
     *
     * @param level New value for {@link #getLevel()}.
     */
    public final synchronized void setDimmableState(final boolean on,
            final float level) {

        try {

            final DeviceState request = new DeviceState();
            request.put("on", on);
            request.put("bri", Math.round(level * 254.0f));
            HueBridge.checkResponse(getBridge().sendPut(request,
                    "lights", getId(), "state"));

        } catch (final IOException e) {

            if (logger.isLoggable(Level.WARNING)) {

                logger.log(Level.WARNING, e.getMessage(), e);

            }
        }
    }

    @Override
    public synchronized boolean update(final DeviceState state,
            final boolean changed) {

        try {

            boolean stateChanged = changed;
            final int bri = (int) state.get("bri");
            final float newLevel = (bri + 1) / 255.0f;

            if (!Utilities.sameFloat(level, newLevel)) {

                level = newLevel;
                stateChanged = true;

            }

            return super.update(state, stateChanged);

        } catch (final Exception e) {

            if (logger.isLoggable(Level.WARNING)) {

                logger.log(Level.WARNING, e.getMessage(), e);

            }
        }

        return changed;

    }
}
