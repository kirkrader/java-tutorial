/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Plugin module for the inversion of control <code>framework</frame> that
 * supports the <a href="https://developers.meethue.com/develop/hue-api/">
 * Philips Hue bridge API </a>.
 *
 * See <a href= "https://developers.meethue.com/develop/hue-api/">
 * https://developers.meethue.com/develop/hue-api/ </a> for information on the
 * Hue API.
 *
 * Note that the code in this package resides in one module in a multi-module
 * Maven project. It depends on the {@code framework} module in the same
 * project. It provides classes that intended to be inject at run time into the
 * inversion of control framework. In other words, while these particular plugin
 * classes were developed as a single Maven project, they can be used as an
 * example of how such plugin modules could be implemented independently in
 * separate Maven projects or using any other build toolchain so long as the
 * {@code framework} module's artifact is available at compile and run time.
 */
package us.rader.example.hue;
