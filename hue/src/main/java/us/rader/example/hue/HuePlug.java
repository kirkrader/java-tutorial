/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;

/**
 * Wrapper for Philips Hue "On/Off plug-in unit" devices.
 *
 * @see HueLightType#PLUG
 */
public class HuePlug extends HueDevice {

    /**
     * {@link Logger} for debugging.
     */
    private static final Logger logger;

    /**
     * Version constant required by {@link java.io.Serializable}.
     */
    private static final long serialVersionUID;

    static {

        serialVersionUID = 1L;
        logger = Logger.getLogger(HuePlug.class.getName());

    }

    /**
     * The on / off state.
     */
    private boolean on;

    /**
     * Initialize {@link #getBridge()} in addition to
     * {@link us.rader.example.framework.impl.DeviceImpl} parameters.
     *
     * @param bridge The {@link HueBridge} that created this instance.
     *
     * @param id     Value for {@link Device#getId()}
     *
     * @param name   Value for {@link Device#getName()}
     *
     * @throws Exception Required by super class declaration.
     */
    public HuePlug(final HueBridge bridge, final String id,
            final String name) throws Exception {

        super(bridge, id, name);

    }

    @Override
    public synchronized DeviceState getState() {

        final DeviceState state = super.getState();
        state.put("on", on);
        return state;

    }

    /**
     * Get the state of this switch.
     *
     * @return {@link #on}
     */
    public final synchronized boolean isOn() {

        return on;

    }

    /**
     * Send PUT request to change the state of this switch.
     *
     * @param on The new switch state.
     */
    public final synchronized void setSwitchableState(
            final boolean on) {

        try {

            final DeviceState request = new DeviceState();
            request.put("on", on);
            HueBridge.checkResponse(getBridge().sendPut(request,
                    "lights", getId(), "state"));

        } catch (final IOException e) {

            if (logger.isLoggable(Level.WARNING)) {

                logger.log(Level.WARNING, e.getMessage(), e);

            }
        }
    }

    @Override
    public synchronized boolean update(final DeviceState state,
            final boolean changed) {

        try {

            boolean stateChanged = changed;
            final boolean newOn = (boolean) state.getOrDefault("on",
                    on);

            if (on != newOn) {

                on = newOn;
                stateChanged = true;

            }

            return super.update(state, stateChanged);

        } catch (final Exception e) {

            if (logger.isLoggable(Level.WARNING)) {

                logger.log(Level.WARNING, e.getMessage(), e);

            }
        }

        return changed;

    }
}
