/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;
import us.rader.example.framework.impl.DeviceImpl;

/**
 * Parent class of Philips Hue devices.
 *
 * @see DeviceImpl
 */
public abstract class HueDevice extends DeviceImpl {

    /**
     * {@link java.util.logging.Logger} used for debugging and analytics.
     */
    private static final Logger logger;

    /**
     * Marshal and un-marshal JSON.
     */
    private static final ObjectMapper mapper;

    /**
     * Version constant required by {@link java.io.Serializable}.
     */
    private static final long serialVersionUID;

    static {

        serialVersionUID = 1L;
        logger = Logger.getLogger(HueDevice.class.getName());
        mapper = new ObjectMapper();

    }

    /**
     * The {@link HueBridge} to use for access to the Hue bridge API.
     */
    private final HueBridge bridge;

    /**
     * Initialize {@link #getBridge()} in addition to
     * {@link us.rader.example.framework.impl.DeviceImpl} parameters.
     *
     * @param bridge The {@link HueBridge} that created this instance.
     *
     * @param id     Value for {@link Device#getId()}
     *
     * @param name   Value for {@link Device#getName()}
     *
     * @throws Exception Required by super class declaration.
     */
    public HueDevice(final HueBridge bridge, final String id,
            final String name) throws Exception {

        super(bridge, id, name);
        this.bridge = bridge;

    }

    /**
     * Get the {@link us.rader.example.hue.HueBridge} wrapper object for this
     * device.
     *
     * @return The {@link us.rader.example.hue.HueBridge} that created this
     *         instance.
     */
    protected synchronized HueBridge getBridge() {

        return bridge;

    }

    /**
     * Invoke the Hue bridge API to alter the name of this device.
     *
     * @param name The new name for the device.
     */
    @Override
    public synchronized final void setName(final String name) {

        try {

            final DeviceState request = new DeviceState();
            request.put("name", name);
            HueBridge.checkResponse(bridge.sendPut(request));
            super.setName(name);

        } catch (final IOException e) {

            if (logger.isLoggable(Level.WARNING)) {

                logger.log(Level.WARNING, e.getMessage(), e);

            }
        }
    }

    @Override
    public boolean update(final DeviceState state,
            final boolean changed) {

        return changed;

    }

    /**
     * Update the device state based on value retrieved by the polling loop.
     *
     * Convert given JsonNode to DeviceState and pass it to
     * {@link #update(DeviceState, boolean)}.
     *
     * @param response The updated device state.
     *
     * @param changed  Indicates whether or not a child class' state changed.
     */
    synchronized final void update(final JsonNode response,
            final boolean changed) {

        if (update(mapper.convertValue(response.required("state"),
                DeviceState.class), changed)) {

            notifyStateChangeListeners();

        }
    }
}
