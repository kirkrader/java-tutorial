/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.logging.Level.FINE;
import static java.util.logging.Level.FINER;
import static java.util.logging.Level.FINEST;
import static java.util.logging.Level.SEVERE;
import static java.util.logging.Level.WARNING;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import us.rader.example.framework.Device;
import us.rader.example.framework.Framework;
import us.rader.example.framework.Group;
import us.rader.example.framework.impl.PluginImpl;
import us.rader.example.framework.impl.SceneKey;

/**
 * <a href="https://developers.meethue.com/develop/hue-api/">Hue bridge API</a>
 * wrapper.
 *
 * @see <a href=
 *      "https://developers.meethue.com/develop/hue-api/">https://developers.meethue.com/develop/hue-api/</a>
 */
public final class HueBridge extends PluginImpl implements Runnable {

    /**
     * {@link java.util.logging.Logger} used for debugging and analytics.
     */
    private static final Logger logger;

    /**
     * Serialize JSON.
     */
    private static final ObjectMapper mapper;

    static {

        logger = Logger.getLogger(HueBridge.class.getName());
        mapper = new ObjectMapper();

    }

    /**
     * Throw {@link java.lang.IllegalArgumentException} if response from device
     * command request is invalid.
     *
     * @param response Response from PUT request.
     *
     * @throws IOException If API encounters a run-time error.
     */
    public static void checkResponse(final JsonNode response)
            throws IOException {

        if (response == null) {
            throw new IllegalArgumentException("null response");
        }

        if (!response.isArray()) {
            throw new IllegalArgumentException("array expected, got "
                    + mapper.writeValueAsString(response));
        }

        final ArrayNode array = (ArrayNode) response;

        for (int index = 0; index < array.size(); ++index) {

            final JsonNode element = array.get(index);

            if (!element.isObject()) {
                throw new IllegalArgumentException(
                        "object expected, got " + mapper
                                .writeValueAsString(response));
            }

            if (element.get("success") == null) {
                throw new IllegalArgumentException(
                        "success expected, got " + mapper
                                .writeValueAsString(response));
            }
        }
    }

    /**
     * Hue bridge IP address
     */
    private final String address;

    /**
     * Map of group instances discovered by querying the bridge API.
     *
     * Keys are the groupss' id's.
     */
    private final Map<String, HueGroup> groups;

    /**
     * Map of light instances discovered by querying the bridge API.
     *
     * Keys are the devices' id's.
     */
    private final Map<String, HueDevice> lights;

    /**
     * Map of scene instances discovered by querying the bridge API.
     *
     * Keys are the scenes' id's.
     */
    private final Map<SceneKey, HueScene> scenes;

    /**
     * Map of sensor instances discovered by querying the bridge API.
     *
     * Keys are the sensors' id's.
     */
    private final Map<String, HueDevice> sensors;

    /**
     * Executor used to poll the bridge API periodically.
     */
    private final ScheduledThreadPoolExecutor timer;

    /**
     * Hue API "user name"
     *
     * @see <a href=
     *      "https://developers.meethue.com/develop/get-started/">https://developers.meethue.com/develop/get-started/</a>
     */
    private final String token;

    /**
     * Initialize API parameters and start polling loop.
     *
     * Use system properties for address, token and polling interval.
     *
     * @param framework The active {@link Framework}.
     *
     * @throws IOException If API encounters a run-time error.
     */
    public HueBridge(final Framework framework) throws IOException {

        this(framework,
                System.getProperty("us.rader.example.hue.address"),
                System.getProperty("us.rader.example.hue.token"));

    }

    /**
     * Initialize API parameters and start polling loop.
     *
     * @param framework The active {@link Framework}.
     *
     * @param address   Hue bridge IP address.
     *
     * @param token     The API token ("user name").
     *
     * @throws IOException If API encounters a run-time error.
     */
    public HueBridge(final Framework framework, final String address,
            final String token) throws IOException {

        super(framework);

        if (address == null) {

            throw new NullPointerException("null address");

        }

        if (token == null) {

            throw new NullPointerException("null token");

        }

        this.address = address;
        this.token = token;
        groups = new HashMap<>();
        lights = new HashMap<>();
        scenes = new HashMap<>();
        sensors = new HashMap<>();
        discoverDevices();
        timer = new ScheduledThreadPoolExecutor(1);
        timer.scheduleAtFixedRate(this, 1, 1, SECONDS);

    }

    /**
     * Stop the polling {@link #timer} and clean up the {@link #lights}.
     *
     * @see #timer
     * @see #lights
     */
    @Override
    public void close() throws Exception {

        stopPollingLoop();
        super.close();

    }

    /**
     * Create {@link HueGroup} instance based on the given Hue API descriptore.
     *
     * @param id    The value for {@link Group#getId()}
     *
     * @param value The descriptor.
     *
     * @return The newly created {@link HueGroup}.
     *
     * @throws Exception There is an error parsing the descriptor.
     */
    private HueGroup createGroup(final String id,
            final JsonNode value) throws Exception {

        final ArrayNode lightIds = (ArrayNode) value
                .required("lights");
        final Device[] members = new Device[lightIds.size()];

        for (int index = 0; index < members.length; ++index) {

            members[index] = lights.get(lightIds.get(index).asText());

        }

        final HueGroup group = new HueGroup(this, id,
                value.required("name").asText(), members);

        if (logger.isLoggable(FINER)) {

            logger.log(FINER, "Created group " + group.toString());

        }

        return group;

    }

    /**
     * Create the {@link HueGroup} instances discovered by querying the Hue bridge
     * API.
     *
     * @param response The response payload from the Hue bridge API.
     */
    private void createGroups(final JsonNode response) {

        synchronized (groups) {

            response.required("groups").fields()
                    .forEachRemaining(entry -> {

                        try {

                            final String id = entry.getKey();
                            final JsonNode value = entry.getValue();
                            final HueGroupType type = HueGroupType
                                    .valueOfType(
                                            value.required("type")
                                                    .asText());

                            if ((type == HueGroupType.ROOM)
                                    || (type == HueGroupType.ZONE)) {

                                groups.put(id,
                                        createGroup(id, value));

                            }

                        } catch (final IllegalArgumentException e) {

                            if (logger.isLoggable(FINEST)) {
                                logger.log(FINEST,
                                        "ignoring error during HueBridge initialization",
                                        e);
                            }

                        } catch (final Exception e) {

                            if (logger.isLoggable(WARNING)) {
                                logger.log(WARNING, e.getMessage(),
                                        e);
                            }
                        }
                    });
        }
    }

    /**
     * Create and initialize an instance of the particular kind of {@link HueDevice}
     * specified by the given Hue metadata.
     *
     * @param id      The device id.
     *
     * @param payload The device descriptor payload.
     *
     * @return The newly created device.
     *
     * @throws Exception Required by super class declaration.
     */
    private HueDevice createLight(final String id,
            final JsonNode payload) throws Exception {

        final HueLightType type = HueLightType
                .valueOfType(payload.required("type").asText());
        HueDevice device;
        final String name = payload.required("name").asText();

        switch (type) {

        case COLOR:
            device = new HueColorLight(this, id, name);
            break;

        case DIMMABLE:
            device = new HueDimmableLight(this, id, name);
            break;

        case MIRED:
            device = new HueMiredLight(this, id, name);
            break;

        case PLUG:
            device = new HuePlug(this, id, name);
            break;

        default:
            throw new IllegalArgumentException(
                    "unsupported device type " + type.type);

        }

        device.update(payload, true);
        return device;

    }

    /**
     * Call {@link #createLight(String, JsonNode)} for each entry in the "lights"
     * field.
     *
     * @param response The response payload from the Hue bridge API.
     */
    private void createLights(final JsonNode response) {

        synchronized (lights) {

            response.required("lights").fields()
                    .forEachRemaining(light -> {

                        final String id = light.getKey();
                        final JsonNode payload = light.getValue();

                        try {

                            final HueDevice device = createLight(id,
                                    payload);
                            lights.put(id, device);

                        } catch (final IllegalArgumentException e) {

                            if (logger.isLoggable(FINEST)) {
                                logger.log(FINEST,
                                        "ignoring error during HueBridge initialization",
                                        e);
                            }

                        } catch (final Exception e) {

                            if (logger.isLoggable(WARNING)) {
                                logger.log(WARNING, e.getMessage(),
                                        e);
                            }
                        }
                    });
        }
    }

    /**
     * Create the {@link HueScene} instances discovered by querying the Hue bridge
     * API.
     *
     * @param response The response payload from the Hue bridge API.
     */
    private void createScenes(final JsonNode response) {

        synchronized (scenes) {

            if (logger.isLoggable(FINER)) {
                logger.log(FINER, "creating scenes");
            }

            response.required("scenes").fields()
                    .forEachRemaining(entry -> {

                        try {

                            final String sceneId = entry.getKey();
                            final JsonNode value = entry.getValue();
                            final String name = value.required("name")
                                    .asText();
                            final String type = value.required("type")
                                    .asText();
                            final boolean recycle = value
                                    .required("recycle").asBoolean();
                            final JsonNode groupIdField = value
                                    .get("group");

                            if (!recycle && "GroupScene".equals(type)
                                    && (groupIdField != null)) {

                                final String groupId = groupIdField
                                        .asText();
                                final HueScene scene = new HueScene(
                                        this, sceneId, name, groupId);

                                scenes.put(
                                        new SceneKey(scene.getId(),
                                                scene.getGroupId()),
                                        scene);

                                if (logger.isLoggable(FINEST)) {
                                    logger.log(FINEST,
                                            "Created scene " + scene
                                                    .toString());
                                }
                            }

                        } catch (final Exception e) {

                            if (logger.isLoggable(WARNING)) {
                                logger.log(WARNING, e.getMessage(),
                                        e);
                            }
                        }
                    });
        }
    }

    /**
     * Create and initialize an instance of the particular kind of {@link HueDevice}
     * specified by the given Hue metadata.
     *
     * @param id      The device id.
     *
     * @param payload The device descriptor payload.
     *
     * @return The newly created device.
     *
     * @throws Exception Required by super class declaration.
     */
    private HueDevice createSensor(final String id,
            final JsonNode payload) throws Exception {

        final HueSensorType type = HueSensorType
                .valueOfType(payload.required("type").asText());
        HueDevice device;
        final String name = payload.required("name").asText();

        switch (type) {

        case TAP:
            device = new HueTap(this, id, name);
            break;

        default:
            throw new IllegalArgumentException(
                    "unsupported device type " + type.type);

        }

        device.update(payload, true);
        return device;

    }

    /**
     * Call {@link #createSensor(String, JsonNode)} for each entry in the "sensors"
     * field.
     *
     * @param response The response payload from the Hue bridge API.
     */
    private void createSensors(final JsonNode response) {

        synchronized (sensors) {

            response.required("sensors").fields()
                    .forEachRemaining(sensor -> {

                        final String id = sensor.getKey();
                        final JsonNode payload = sensor.getValue();

                        try {

                            final HueDevice device = createSensor(id,
                                    payload);
                            sensors.put(id, device);

                        } catch (final IllegalArgumentException e) {

                            if (logger.isLoggable(FINEST)) {

                                logger.log(FINEST,
                                        "ignoring error during HueBridge initialization",
                                        e);

                            }

                        } catch (final Exception e) {

                            if (logger.isLoggable(WARNING)) {

                                logger.log(WARNING, e.getMessage(),
                                        e);

                            }
                        }
                    });
        }
    }

    /**
     * Query the bridge API to populate the {@link #lights} map.
     *
     * Called before starting polling loop thread.
     *
     * @throws IOException If API encounters a run-time error.
     *
     * @see #sendGet(String...)
     * @see HueLightType
     * @see HuePlug
     * @see HueDimmableLight
     * @see HueColorLight
     */
    private void discoverDevices() throws IOException {

        final JsonNode response = sendGet();

        if (logger.isLoggable(FINEST)) {
            logger.log(FINEST, mapper.writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response.required("groups")));
        }

        createLights(response);
        createGroups(response);
        createScenes(response);
        createSensors(response);

    }

    /**
     * Construct the API endpoint URL for a specified request.
     *
     * @param uri URI path components for the particular endpoint.
     *
     * @return The URL.
     *
     * @throws IOException If API encounters a run-time error.
     *
     * @see #address
     * @see #token
     */
    private URL getURL(final String... uri) throws IOException {

        final StringBuilder builder = new StringBuilder("http://");
        builder.append(address);
        builder.append("/api/");
        builder.append(token);

        for (final String path : uri) {
            builder.append("/");
            builder.append(path);
        }

        return new URL(builder.toString());

    }

    /**
     * Process an entry from the "groups" value in the Hue API response.
     *
     * @param entry The entry for the given group.
     */
    private void processGroup(final Entry<String, JsonNode> entry) {

        synchronized (groups) {

            final HueGroup group = groups.get(entry.getKey());

            if (group != null) {

                group.update(entry.getValue());

            }
        }
    }

    /**
     * Process an entry from the "lights" value in the Hue API response.
     *
     * Calls {@link HueDevice#update(JsonNode, boolean)} if the id matches an entry
     * in {@link #lights}.
     *
     * @param entry The entry for the given light.
     */
    private void processLight(final Entry<String, JsonNode> entry) {

        synchronized (lights) {

            final HueDevice device = lights.get(entry.getKey());

            if (device != null) {

                device.update(entry.getValue(), false);

            }
        }
    }

    /**
     * Process an entry from the "sensors" value in the Hue API response.
     *
     * Calls {@link HueDevice#update(JsonNode, boolean)} if the id matches an entry
     * in {@link #sensors}.
     *
     * @param entry The entry for the given sensor.
     */
    private void processSensor(final Entry<String, JsonNode> entry) {

        synchronized (sensors) {

            final HueDevice device = sensors.get(entry.getKey());

            if (device != null) {

                device.update(entry.getValue(), false);

            }
        }
    }

    /**
     * The polling task called periodically by the {@link #timer}.
     *
     * 1. call {@link #sendGet(String ...)} for current state of all devices and
     * groups from the bridge
     *
     * 2. call {@link #processLight(Entry)} for each key/value pair in the "lights"
     * field
     *
     * @see #lights
     * @see #lights
     * @see #lights
     * @see #lights
     */
    @Override
    public void run() {

        try {

            if (logger.isLoggable(FINER)) {
                logger.log(FINER, "polling Hue bridge");
            }

            final JsonNode response = sendGet();

            synchronized (lights) {

                response.required("lights").fields()
                        .forEachRemaining(this::processLight);

            }

            synchronized (groups) {

                response.required("groups").fields()
                        .forEachRemaining(this::processGroup);

            }

            synchronized (sensors) {

                response.required("sensors").fields()
                        .forEachRemaining(this::processSensor);

            }

        } catch (final Exception e) {

            logger.log(SEVERE, "error fetching Hue lights", e);

        }
    }

    /**
     * Send a GET request to the Hue API.
     *
     * @param uri URI path components for the particular endpoint.
     *
     * @return The response payload.
     *
     * @throws IOException If API encounters a run-time error.
     *
     * @see #getURL(String...)
     * @see #sendPut(Object, String...)
     */
    JsonNode sendGet(final String... uri) throws IOException {

        final URL url = getURL(uri);

        if (logger.isLoggable(FINER)) {
            logger.log(FINER,
                    "sending GET request to " + url.toExternalForm());
        }

        final HttpURLConnection connection = (HttpURLConnection) url
                .openConnection();
        connection.connect();

        try {

            try (InputStream input = connection.getInputStream()) {

                final JsonNode response = mapper.readTree(input);

                if (logger.isLoggable(FINER)) {
                    logger.log(FINER, "GET response: "
                            + mapper.writeValueAsString(response));
                }

                return response;

            }

        } finally {

            connection.disconnect();

        }
    }

    /**
     * Send PUT request to Hue bridge API endpoint.
     *
     * @param request The request payload.
     *
     * @param uri     URI path components for the particular endpoint.
     *
     * @return The response payload.
     *
     * @throws IOException If API encounters a run-time error.
     *
     * @see #getURL(String...)
     * @see #sendGet(String ...)
     */
    JsonNode sendPut(final Object request, final String... uri)
            throws IOException {

        final URL url = getURL(uri);

        if (logger.isLoggable(FINER)) {
            logger.log(FINER,
                    "sending PUT request to " + url.toExternalForm()
                            + " with payload " + request.toString());
        }

        final HttpURLConnection connection = (HttpURLConnection) url
                .openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("PUT");
        connection.connect();

        try {

            try (OutputStream output = connection.getOutputStream()) {
                mapper.writeValue(output, request);
            }

            try (InputStream input = connection.getInputStream()) {

                final JsonNode response = mapper.readTree(input);

                if (logger.isLoggable(FINER)) {
                    logger.log(FINER, "PUT response: "
                            + mapper.writeValueAsString(response));
                }

                return response;

            }

        } finally {

            connection.disconnect();

        }
    }

    /**
     * Stop the polling loop.
     */
    private void stopPollingLoop() {

        try {

            timer.shutdown();

            if (!timer.awaitTermination(5, SECONDS)) {

                logger.log(SEVERE,
                        "Polling thread failed to terminate cleanly");
                timer.shutdownNow();

            }

        } catch (final InterruptedException e) {

            logger.log(SEVERE, "interrupted while closing HueBridge",
                    e);

        } finally {

            if (logger.isLoggable(FINE)) {
                logger.log(FINE,
                        "Hue bridge polling loop terminated");
            }
        }
    }
}
