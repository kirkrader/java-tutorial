/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

/**
 * The supported kinds of Hue lights.
 *
 * The strings used for {@link #type} and with {@link #valueOfType(String)}
 * correspond the "type" fields in the metadata for lights returned by the Hue
 * bridge API.
 *
 * Hue lights with "type" value not appearing here are ignored.
 *
 * @author kirk
 * @version $Id: $Id
 */
public enum HueLightType {

    /**
     * Light of type "Extended color light".
     */
    COLOR("Extended color light"),

    /**
     * Light of type "Dimmable light".
     */
    DIMMABLE("Dimmable light"),

    /**
     * Light of type "Color temperature light".
     */
    MIRED("Color temperature light"),

    /**
     * Light of type "On/Off plug-in unit".
     */
    PLUG("On/Off plug-in unit");

    /**
     * Return the {@link HueLightType} value denoted by the given "type" string.
     *
     * @param type The "type" string.
     *
     * @return The selected {@link HueLightType}.
     */
    public static HueLightType valueOfType(final String type) {

        for (final HueLightType value : values()) {

            if (value.type.equals(type)) {

                return value;

            }
        }

        throw new IllegalArgumentException(
                "unsupported Hue light type:" + type);

    }

    /**
     * String used in the {@code type} field returned for lights by the Hue bridge
     * API.
     */
    public final String type;

    /**
     * Initialize the {@code type} string.
     *
     * @param type The string used in the {@code type} field by the Hue bridge API.
     */
    private HueLightType(final String type) {

        this.type = type;

    }
}
