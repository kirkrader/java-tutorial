/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

import us.rader.example.framework.DeviceState;
import us.rader.example.framework.Scene;
import us.rader.example.framework.impl.SceneImpl;

/**
 * Hue implementation of {@link Scene}.
 */
public final class HueScene extends SceneImpl {

    /**
     * Version number constant required by {@link java.io.Serializable}.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The {@link HueBridge}.
     */
    private final HueBridge bridge;

    /**
     * Invoke super-constructor.
     *
     * @param bridge  The {@link HueBridge}.
     *
     * @param id      This scene's id.
     *
     * @param name    This scene's name.
     *
     * @param groupId The id of this scene's group.
     */
    public HueScene(final HueBridge bridge, final String id,
            final String name, final String groupId) {

        super(bridge, id, name, groupId);

        if (bridge == null) {
            throw new NullPointerException("null bridge");
        }

        this.bridge = bridge;

    }

    @Override
    public synchronized void activate() throws Exception {

        final DeviceState request = new DeviceState();
        request.put("scene", getId());
        HueBridge.checkResponse(bridge.sendPut(request, "groups",
                getGroupId(), "action"));
        super.activate();

    }

    @Override
    public synchronized void setName(final String name) {

        // TODO not yet implemented

        super.setName(name);

    }

    @Override
    public boolean update(final DeviceState state,
            final boolean changed) {

        // TODO Auto-generated method stub

        return changed;

    }
}
