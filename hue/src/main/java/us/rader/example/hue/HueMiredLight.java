/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;

/**
 * {@link HueDevice} for "Hue White Ambiance" lights.
 */
public class HueMiredLight extends HueDimmableLight {

    /**
     * {@link Logger} for debugging.
     */
    private static final Logger logger;

    /**
     * Version constant required by {@link java.io.Serializable}.
     */
    private static final long serialVersionUID;

    static {

        serialVersionUID = 1L;
        logger = Logger.getLogger(HueDimmableLight.class.getName());

    }

    /**
     * The color temperature of this light in mireds.
     */
    private int colorTemperature;

    /**
     * Initialize super class.
     *
     * @param bridge The {@link HueBridge}.
     *
     * @param id     The value for {@link Device#getId()}.
     *
     * @param name   The value for {@link Device#getName()}.
     *
     * @throws Exception If thrown by super-constructor.
     */
    public HueMiredLight(final HueBridge bridge, final String id,
            final String name) throws Exception {

        super(bridge, id, name);

    }

    /**
     * Get the color temperature in mireds.
     *
     * @return {@link #colorTemperature}
     */
    public final synchronized int getColorTemperature() {

        return colorTemperature;

    }

    @Override
    public synchronized DeviceState getState() {

        final DeviceState state = super.getState();
        state.put("colorTemperature", colorTemperature);
        return state;

    }

    /**
     * Send PUT request to change the state of this light.
     *
     * @param on               New value for {@link HuePlug#isOn()}.
     *
     * @param level            New value for {@link HueDimmableLight#getLevel()}.
     *
     * @param colorTemperature New value for {@link #colorTemperature}.
     */
    public final synchronized void setMiredTunableState(
            final boolean on, final float level,
            final int colorTemperature) {

        try {

            final DeviceState request = new DeviceState();
            request.put("on", on);
            request.put("bri", Math.round(level * 254.0f));
            request.put("ct", colorTemperature);
            HueBridge.checkResponse(getBridge().sendPut(request,
                    "lights", getId(), "state"));

        } catch (final IOException e) {

            if (logger.isLoggable(Level.WARNING)) {

                logger.log(Level.WARNING, e.getMessage(), e);

            }
        }
    }

    @Override
    public synchronized boolean update(final DeviceState state,
            final boolean changed) {

        try {

            boolean stateChanged = changed;
            final int newColorTemperature = (int) state.get("ct");

            if (colorTemperature != newColorTemperature) {

                colorTemperature = newColorTemperature;
                stateChanged = true;

            }

            return super.update(state, stateChanged);

        } catch (final Exception e) {

            if (logger.isLoggable(Level.WARNING)) {

                logger.log(Level.WARNING, e.getMessage(), e);

            }
        }

        return changed;

    }
}
