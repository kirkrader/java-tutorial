/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

import com.fasterxml.jackson.databind.JsonNode;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;
import us.rader.example.framework.Group;
import us.rader.example.framework.impl.GroupImpl;
import us.rader.example.framework.impl.Utilities;

/**
 * {@link Group} of Philips Hue devices.
 */
public final class HueGroup extends GroupImpl {

    /**
     * Required serial version constant.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Average color temperature of mired tunable members.
     */
    private int colorTemperature;

    /**
     * Average hue of colorable members.
     */
    private float hue;

    /**
     * Average level of dimmable members.
     */
    private float level;

    /**
     * Any member is on.
     */
    private boolean on;

    /**
     * Average saturation of colorable members.
     */
    private float saturation;

    /**
     * Invoke super-constructor.
     *
     * @param bridge  The {@link HueBridge}.
     *
     * @param id      Group's id.
     *
     * @param name    Group's name.
     *
     * @param members Initial set of Group's members.
     *
     * @throws Exception Required by super class declaration.
     */
    public HueGroup(final HueBridge bridge, final String id,
            final String name, final Device... members)
            throws Exception {

        super(bridge, id, name, members);

    }

    @Override
    public synchronized DeviceState getState() {

        final DeviceState state = super.getState();
        state.put("on", on);
        state.put("level", level);
        state.put("colorTemperature", colorTemperature);
        state.put("hue", hue);
        state.put("saturation", saturation);
        return state;

    }

    @Override
    public synchronized void setName(final String name) {

        // TODO not yet implemented

        super.setName(name);

    }

    @Override
    public boolean update(final DeviceState state,
            final boolean changed) {

        boolean stateChanged = changed;
        final boolean newOn = (boolean) state.get("on");
        final float newLevel = (float) state.get("level");
        final int newColorTemperature = (int) state
                .get("colorTemperature");
        final float newHue = (float) state.get("hue");
        final float newSaturation = (float) state.get("saturation");

        if (!((on == newOn) && Utilities.sameFloat(level, newLevel)
                && (colorTemperature == newColorTemperature)
                && Utilities.sameFloat(hue, newHue)
                && Utilities.sameFloat(saturation, newSaturation))) {

            on = newOn;
            level = newLevel;
            colorTemperature = newColorTemperature;
            hue = newHue;
            saturation = newSaturation;
            stateChanged = true;

        }

        return stateChanged;

    }

    /**
     * Update {@link Group} state based on response from Hue API.
     *
     * @param response Descriptor from Hue API.
     */
    public final void update(final JsonNode response) {

        final DeviceState state = getState();
        final JsonNode action = response.required("action");
        final JsonNode onField = action.get("on");
        final JsonNode briField = action.get("bri");
        final JsonNode ctField = action.get("ct");
        final JsonNode hueField = action.get("hue");
        final JsonNode satField = action.get("sat");

        state.put("on", (onField == null) ? on : onField.asBoolean());
        state.put("level", (briField == null) ? level
                : ((briField.asInt()) + 1) / 255.0f);
        state.put("colorTemperature",
                (ctField == null) ? colorTemperature
                        : ctField.asInt());
        state.put("hue", (hueField == null) ? hue
                : ((hueField.asInt()) + 1) / 65536.0f);
        state.put("saturation", (satField == null) ? saturation
                : ((satField.asInt()) + 1) / 255.0f);

        if (update(state, false)) {

            notifyStateChangeListeners();

        }

    }
}
