/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

/**
 * The supported kinds of Hue sensors.
 *
 * The strings used for {@link #type} and with {@link #valueOfType(String)}
 * correspond the "type" fields in the metadata for groups returned by the Hue
 * bridge API.
 *
 * Hue devices with "type" value not appearing here are ignored.
 */
public enum HueSensorType {

    /**
     * Sensor of type "ZGPSwitch".
     */
    TAP("ZGPSwitch");

    /**
     * Return the {@link HueGroupType} value denoted by the given "type" string.
     *
     * @param type The "type" string.
     *
     * @return The selected {@link HueGroupType}.
     */
    public static HueSensorType valueOfType(final String type) {

        for (final HueSensorType value : values()) {

            if (value.type.equals(type)) {
                return value;
            }
        }

        throw new IllegalArgumentException(
                "unsupported Hue group type:" + type);

    }

    /**
     * String used in the "type" field returned for groups by the Hue bridge API.
     */
    public final String type;

    /**
     * Initialize the {@code type} string.
     *
     * @param type The string used in the {@code type} field by the Hue bridge API.
     */
    private HueSensorType(final String type) {
        this.type = type;
    }
}
