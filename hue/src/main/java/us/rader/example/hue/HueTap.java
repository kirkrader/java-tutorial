/*
 * Copyright (C) 2020 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.example.hue;

import static java.util.logging.Level.WARNING;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import us.rader.example.framework.Device;
import us.rader.example.framework.DeviceState;

/**
 * Push-button devices.
 */
public class HueTap extends HueDevice {

    /**
     * {@link SimpleDateFormat} used to parse "lastupdated" field.
     */
    private static final SimpleDateFormat format;

    /**
     * {@link Logger} used for debug output.
     */
    private static final Logger logger;

    /**
     * Version constant required by {@link Serializable}.
     */
    private static final long serialVersionUID = 1L;

    static {

        logger = Logger.getLogger(HueTap.class.getName());
        format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    }

    /**
     * Map raw "buttonevent" values to comprehensible button numbers.
     *
     * Returns the given number if it is not one of the canonical values to account
     * for oddities of the Hue API. For example, a Tap device will report a
     * {@code null} "buttonevent" when it has just been added to the Zigbee network
     * but no button has yet been pressed. This becomes a 0 in the output from
     * {@code JsonNode::asInt()}.
     *
     * @param value The raw value of the "buttonevent" field from the Hue API.
     *
     * @return Button number in the range 1 - 4 or the original value if not
     *         recognized.
     */
    private static int buttonNumber(final int value) {

        switch (value) {

        case 34:

            return 1;

        case 16:

            return 2;

        case 17:

            return 3;

        case 18:

            return 4;

        default:

            return value;

        }
    }

    /**
     * The id of the last button pressed.
     */
    private int button;

    /**
     * The time of last button press event.
     *
     * @see #button
     */
    private Date lastUpdated;

    /**
     * Initialize super class.
     *
     * @param bridge The {@link HueBridge}.
     *
     * @param id     The value for {@link Device#getId()}.
     *
     * @param name   The value for {@link Device#getName()}.
     *
     * @throws Exception If thrown by
     *                   {@link HueDevice#HueDevice(HueBridge, String, String)}.
     */
    public HueTap(final HueBridge bridge, final String id,
            final String name) throws Exception {

        super(bridge, id, name);

    }

    @Override
    public synchronized DeviceState getState() {

        final DeviceState state = super.getState();
        state.put("button", button);
        return state;

    }

    @Override
    public synchronized boolean update(final DeviceState state,
            final boolean changed) {

        try {

            boolean stateChanged = changed;
            final String lastUpdatedString = (String) state
                    .get("lastupdated");
            final Date newLastUpdated = ("none"
                    .equals(lastUpdatedString) ? new Date(0)
                            : format.parse(lastUpdatedString));
            final int newButton = buttonNumber(
                    (int) state.get("buttonevent"));

            if (!((newButton == button)
                    && newLastUpdated.equals(lastUpdated))) {

                button = newButton;
                lastUpdated = newLastUpdated;
                stateChanged = true;

            }

            return super.update(state, stateChanged);

        } catch (final ParseException e) {

            if (logger.isLoggable(WARNING)) {

                logger.log(WARNING, e.getMessage(), e);

            }
        }

        return false;

    }
}
